import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import { appState } from '@/plugins/stateWatcher';

export default new Vuex.Store({
  state: {
    // email: 'renen@121.co.za',
    // otp: 'xxx',
    // apiKey: 'c2c30289-192a-42a3-a360-645158ef8795',
    // customerName: 'Investec',
    // rules: [{"id":7,"ruleSetId":7,"name":"Woolworths","minimumAmount":null,"maximumAmount":"0.5e3","currency":null,"ruleAppliesOnlyToMerchantRegex":"Wool","ruleDoesntApplyToMerchantRegex":null,"allowedMerchantNameRegex":null,"blockedMerchantNameRegex":null,"allowedCategories":null,"blockedCategories":null,"allowedCities":null,"blockedCities":null,"country":null},{"id":8,"ruleSetId":7,"name":"Pick n Pay","minimumAmount":null,"maximumAmount":"0.3e3","currency":null,"ruleAppliesOnlyToMerchantRegex":"Pick","ruleDoesntApplyToMerchantRegex":null,"allowedMerchantNameRegex":null,"blockedMerchantNameRegex":null,"allowedCategories":null,"blockedCategories":null,"allowedCities":null,"blockedCities":null,"country":null},{"id":9,"ruleSetId":7,"name":"Max for non-food retailers","minimumAmount":null,"maximumAmount":"0.15e3","currency":null,"ruleAppliesOnlyToMerchantRegex":null,"ruleDoesntApplyToMerchantRegex":"Pick|Wool","allowedMerchantNameRegex":null,"blockedMerchantNameRegex":null,"allowedCategories":null,"blockedCategories":null,"allowedCities":null,"blockedCities":null,"country":null},{"id":10,"ruleSetId":7,"name":"Mavericks","minimumAmount":null,"maximumAmount":null,"currency":null,"ruleAppliesOnlyToMerchantRegex":null,"ruleDoesntApplyToMerchantRegex":null,"allowedMerchantNameRegex":null,"blockedMerchantNameRegex":"Mavericks","allowedCategories":null,"blockedCategories":null,"allowedCities":null,"blockedCities":null,"country":null},{"id":11,"ruleSetId":7,"name":"No Booze","minimumAmount":null,"maximumAmount":null,"currency":null,"ruleAppliesOnlyToMerchantRegex":null,"ruleDoesntApplyToMerchantRegex":null,"allowedMerchantNameRegex":null,"blockedMerchantNameRegex":null,"allowedCategories":null,"blockedCategories":"[\"liquor\", \"booze\"]","allowedCities":null,"blockedCities":null,"country":null}],
    // cards: [{"id":7,"investecAccountNumber":"10010159833","cardHolderName":"SE Watermeyer"},{"id":8,"investecAccountNumber":"10010161215","cardHolderName":"JE Smith"},{"id":9,"investecAccountNumber":"10010161215","cardHolderName":"JE Smith2"},{"id":10,"investecAccountNumber":"10010161215","cardHolderName":"R Watermeyer2"},{"id":11,"investecAccountNumber":"10010161215","cardHolderName":"R Watermeyer23"}],
    // ruleSets: [{"id":7,"name":"Pocket Money Rules"}],

    email: null,
    otp: null,
    apiKey: null,

    customerName: null,

    postUrl: null,
    slackUrl: null,

    cards: [],
    ruleSets: [],
    rules: [],
    transactions: [],
    cardRuleSets: {},

    newRule: null,
    newCard: null,
    newRuleSet: null,
    updatedCardRuleSets: null,
    currentTransactionId: null,
    currentChecks: [],

  },
  mutations: {
    setEmail(state, email) { state.email = email; },
    setOtp(state, otp) { state.otp = otp; },
    setAuth(state, data) {
      state.apiKey = data['apiKey'];
      state.postUrl = data['postUrl'];
      state.slackUrl = data['slackUrl'];
      state.customerName = data['name'];
    },
    setCustomerName(state, customerName) { state.customerName = customerName; },
    setPostUrl(state, postUrl) { state.postUrl = postUrl; },
    setSlackUrl(state, slackUrl) { state.slackUrl = slackUrl; },
    setCards(state, cards) { state.cards = cards; },
    setTransactions(state, transactions) { state.transactions = transactions; },
    setRuleSets(state, ruleSets) { state.ruleSets = ruleSets; },
    newRuleSet(state, ruleSet) { state.newRuleSet = ruleSet; },
    addRuleSet(state, ruleSetId) {
      state.ruleSets.unshift({
        id: ruleSetId,
        name: state.newRuleSet['name'],
      });
    },
    setRules(state, rules) { state.rules = rules; },
    newRule(state, rule) { state.newRule = rule; },
    newCard(state, card) { state.newCard = card; },
    addCard(state, cardId) {
      let n = state.newCard;
      n['id'] = cardId;
      state.cards.unshift(n);
    },
    setCardRuleSets(state, cardRuleSets) { state.cardRuleSets = cardRuleSets; },
    updateCardRuleSets(state, cardRuleSets) {
      state.updatedCardRuleSets = cardRuleSets;
      state.cardRuleSets[cardRuleSets['cardId']] = cardRuleSets['ruleSetIds'];
    },
    setCurrentTransactionId(state, currentTransactionId) { state.currentTransactionId = currentTransactionId },
    setCurrentChecks(state, currentChecks) { state.currentChecks = currentChecks },
  },
  actions: {
    setEmail(context, email) { context.commit('setEmail', email); },
    setOtp(context, otp) { context.commit('setOtp', otp); },
    setCustomerName(context, name) {context.commit('setCustomerName', name); },
    setPostUrl(context, postUrl) {context.commit('setPostUrl', postUrl); },
    setSlackUrl(context, slackUrl) {context.commit('setSlackUrl', slackUrl); },
    newRule(context, rule) { context.commit('newRule', rule) },
    newCard(context, card) { context.commit('newCard', card) },
    newRuleSet(context, ruleSet) { context.commit('newRuleSet', ruleSet) },
    updateCardRuleSets(context, cardRuleSets) { context.commit('updateCardRuleSets', cardRuleSets) },
    setCurrentTransactionId(context, currentTransactionId) { context.commit('setCurrentTransactionId', currentTransactionId) },
  },
  getters: {
    email(state) { return state.email; },
    otp(state) { return state.otp; },
    apiKey(state) { return state.apiKey; },
    customerName(state) { return state.customerName; },
    postUrl(state) { return state.postUrl; },
    slackUrl(state) { return state.slackUrl; },
    ruleSets(state) { return state.ruleSets; },
    rules(state) { return state.rules; },
    cards(state) { return state.cards; },
    transactions(state) { return state.transactions },
    cardRuleSets(state) { return state.cardRuleSets },
    currentChecks(state) { return state.currentChecks },
  },
  modules: {
  },
  plugins: [ appState ],
})
