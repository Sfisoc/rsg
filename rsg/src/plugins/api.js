const api = () => {

  const baseURL = 'https://k7et9syyb3.execute-api.eu-west-1.amazonaws.com/Prod/'

  const fetchDataFromApi = async (apiCall, options) => {
    const url = `${baseURL}${apiCall}`
    console.log(url);
    const response = await fetch(url, options);
    let json = null;
    if (response.ok) {
      const text = await response.text();
      if (text.length > 1) {
        json = JSON.parse(text);
      }
    }
    return { success: response.ok, status: response.status, json };
  };

  function sendOtp(email) {
    return fetchDataFromApi(`sendOtp?email=${email}`);
  }

}

export default { api };


// function authenticate(apiKey) {
//   return fetchDataFromApi(`${baseURL}/authenticate?app_key=${apiKey}`);
// }

// function status(apiKey, venueId) {
//   return fetchDataFromApi(`${baseURL}/status?app_key=${apiKey}&venue_id=${venueId}`);
// }

// function lines(apiKey, venueId) {
//   return fetchDataFromApi(`${baseURL}/lines?app_key=${apiKey}&venue_id=${venueId}`);
// }

// function venueOverview(apiKey, venueId) {
//   return fetchDataFromApi(`${baseURL}/venue_overview?app_key=${apiKey}&venue_id=${venueId}`);
// }

// function daysDispenseSummary(apiKey, venueId, offset) {
//   return fetchDataFromApi(`${baseURL}/days_dispense_summary?app_key=${apiKey}&venue_id=${venueId}&offset=${offset}`);
// }

// function daysDispenseHistory(apiKey, venueId, offset, liquid, tipple) {
//   return fetchDataFromApi(`${baseURL}/days_dispense_history?app_key=${apiKey}&venue_id=${venueId}&offset=${offset}&liquid=${liquid}&tipple=${tipple}`);
// }

// function brandHistory(apiKey, venueId, liquid, lastDispenseId) {
//   return fetchDataFromApi(`${baseURL}/brand_history?app_key=${apiKey}&venue_id=${venueId}&liquid=${liquid}&last_dispense_id=${lastDispenseId}`);
// }

