import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import Cards from '@/views/Cards.vue'
import RuleSets from '@/views/RuleSets.vue'
import Settings from '@/views/Settings.vue'
import Transactions from '@/views/Transactions.vue'
import Checks from '@/views/Checks.vue'
import CardRegistration from '@/views/CardRegistration.vue'
import CardRuleSets from '@/views/CardRuleSets.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/cards',
    name: 'Cards',
    component: Cards
  },
  {
    path: '/rule_sets',
    name: 'RuleSets',
    component: RuleSets
  },
  {
    path: '/transactions',
    name: 'Transactions',
    component: Transactions
  },
  {
    path: '/checks',
    name: 'Checks',
    component: Checks
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings
  },
  {
    path: '/cardRegistration',
    name: 'CardRegistration',
    component: CardRegistration
  },
  {
    path: '/card_rule_sets',
    name: 'CardRuleSets',
    component: CardRuleSets
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
