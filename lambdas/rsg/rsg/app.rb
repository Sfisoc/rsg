require 'json'
require 'mysql2'
require 'sequel'
require 'aws-sdk-ssm'
require 'aws-sdk-ses'
require 'net/http'
require 'uri'
# require 'bigdecimal/util'

AWS_REGION     = 'eu-west-1'

# NB NB: start with this article, do not google!
# to build this for deployment, use these instructions: https://blog.shikisoft.com/ruby-aws-lambda-sam-cli-rds-mysql/

# docker run -v "$PWD":/var/task -it lambci/lambda:build-ruby2.7 /bin/bash
# bundle update
# sam build --use-container
# sam deploy --guided

# to test: from the lambdas directory: ./run.rb rsg assess.json

connection_params = { host: 'localhost', username: 'lambda', password: 'lambda', database: 'rsg', port: 3306, connect_timeout: 5 }

if $debug.nil?
  ssm = Aws::SSM::Client.new(region: AWS_REGION)
  env = ssm.get_parameters({ names: %w(RSG_DB_HOST RSG_DB_PASSWORD RSG_DB_USER) })[:parameters].map { |e| [e[:name], e[:value]] }.to_h
  connection_params = {
                        host:     env["RSG_DB_HOST"],
                        username: env["RSG_DB_USER"],
                        password: env["RSG_DB_PASSWORD"],
                        database: 'rsg',
                        port:     3306,
                        connect_timeout: 5,
                      }
end

Sequel.default_timezone = :utc
DB = Sequel.mysql2(**connection_params)

if !$debug.nil?
  require 'logger'
  DB.loggers << Logger.new($stdout)
end



def lambda_handler(event:, context:)
  puts event.to_json
  path = event['path']
  body = event['body']
  json = ( body ? JSON.parse(body) : nil )
  qs   = event['queryStringParameters']
  puts path
  response = case path
  when '/assess'         then assess_request(json)
  when '/post'           then post_request(json)
  when '/sendOtp'        then otp(qs)
  when '/auth'           then auth(qs)
  when '/updateCustomer' then updateCustomer(qs)
  when '/ruleSets'       then rule_sets(qs)
  when '/newRuleSet'     then new_rule_sets(qs)
  when '/rules'          then rules(qs)
  when '/newRule'        then new_rule(qs)
  when '/cards'          then cards(qs)
  when '/newCard'        then new_card(qs)
  when '/transactions'   then transactions(qs)
  when '/cardRuleSets'   then cardRuleSets(qs)
  when '/updateCardRuleSets' then updateCardRuleSets(qs)
  when '/checks'         then checks(qs)
  end
  puts response
  if $debug.nil?
    {
      statusCode: 200,
      headers: { "Access-Control-Allow-Origin" => "*" },
      body: response.to_json
    }
  else
    pp response
  end
end

def checks(qs)
  customer_id = lookup_customer_id(qs['apiKey'])
  if customer_id
    DB[:Checks].where(transactionId: qs['transactionId']).where(customerId: customer_id).all
  end
end

def updateCardRuleSets(qs)
  customer_id = lookup_customer_id(qs['apiKey'])
  if customer_id
    card_id      = qs['cardId'].to_i
    rule_set_ids = qs['ruleSetIds']
    puts rule_set_ids
    DB[:CardRuleSets].where(customerId: customer_id).where(cardId: card_id).delete()
    (rule_set_ids || '').split(',').map(&:to_i).each do |id|
      DB[:CardRuleSets].insert(customerId: customer_id, cardId: card_id, ruleSetId: id, enabled: true, createdAt: Time.now, updatedAt: Time.now)
    end
  end
end

def cardRuleSets(qs)
  customer_id = lookup_customer_id(qs['apiKey'])
  if customer_id
    DB[:CardRuleSets].where(customerId: customer_id).where(enabled: true).select(:cardId, :ruleSetId).all.group_by { |e| e[:cardId] }.transform_values { |v| v.map { |f| f[:ruleSetId]} }
  end
end

def transactions(qs)
  customer_id = lookup_customer_id(qs['apiKey'])
  if customer_id
    DB[:Transactions].where(customerId: customer_id).select(:id, :createdAt, :customerId, :cardId, :investecAccountNumber, :reference, :amount, :currency, :merchantName, :category, :city, :country, :allowed).all
  end
end

def new_rule_sets(qs)
  customer_id = lookup_customer_id(qs['apiKey'])
  rule_set    = qs.tap { |e| e.delete('apiKey') }.merge(customerId: customer_id, updatedAt: Time.now, createdAt: Time.now)
  id          = DB[:RuleSets].insert(rule_set)  if customer_id
  { id: id }
end

def rule_sets(qs)
  customer_id = lookup_customer_id(qs['apiKey'])
  if customer_id
    DB[:RuleSets].where(customerId: customer_id).where(deleted: false).select(:id, :name).all
  end
end

def new_card(qs)
  customer_id = lookup_customer_id(qs['apiKey'])
  card        = qs.tap { |e| e.delete('apiKey') }.merge(customerId: customer_id, updatedAt: Time.now, createdAt: Time.now)
  id          = DB[:Cards].insert(card)  if customer_id
  { id: id }
end

def cards(qs)
  customer_id = lookup_customer_id(qs['apiKey'])
  if customer_id
    DB[:Cards].where(customerId: customer_id).select(:id, :investecAccountNumber, :cardHolderName).all
  end
end

def new_rule(qs)
  customer_id = lookup_customer_id(qs['apiKey'])
  rule        = qs.tap { |e| e.delete('apiKey') }.merge(customerId: customer_id, updatedAt: Time.now, createdAt: Time.now)
  id          = DB[:Rules].insert(rule)  if customer_id
  { id: id }
end

def rules(qs)
  customer_id = lookup_customer_id(qs['apiKey'])
  if customer_id
    DB[:Rules]
      .join(:RuleSets, id: :ruleSetId)
      .where(Sequel.lit('RuleSets.customerId = ?', customer_id))
      .where(deleted: false)
      .select(Sequel.lit('Rules.id').as(:id), :ruleSetId, Sequel.lit('Rules.name').as(:name), :minimumAmount, :maximumAmount, :currency, :ruleAppliesOnlyToMerchantRegex,
                :ruleDoesntApplyToMerchantRegex, :allowedMerchantNameRegex, :blockedMerchantNameRegex, :allowedCategories,
                :blockedCategories, :allowedCities, :blockedCities, :country)
      .all
  else
    []
  end
end

def updateCustomer(qs)
  customer_id = lookup_customer_id(qs['apiKey'])
  changes = {}
  changes[:name]     = qs['customerName']     if qs['customerName']
  changes[:postUrl]  = qs['postUrl']          if qs['postUrl']
  changes[:slackUrl] = qs['slackUrl']         if qs['slackUrl']
  DB[:Customers].where(id: customer_id).update(changes)  if customer_id
end

def lookup_customer_id(api_key)
  (DB[:Users].where(apiKey: api_key).select(:customerId).first || {})[:customerId]
end

def auth(qs)
  r     = nil
  email = qs['email']
  otp   = qs['otp']
  key   = SecureRandom.uuid
  n     = DB[:Users].where(email: email).where(otp: otp).update(otp: nil, apiKey: key)
  if (n==1)
    r = DB[:Users].where(email: email).where(apiKey: key).join(:Customers, id: :customerId).select(:apiKey, :slackUrl, :postUrl, :name).all.first
  end
  r
end

def otp(qs)
  recipient = qs['email']
  otp       = get_otp(recipient)
  send_otp(recipient, otp)
  nil
end

def send_otp(recipient, otp)
  ses       = Aws::SES::Client.new(region: AWS_REGION)
  resp      = ses.send_email({
    destination: { to_addresses: [recipient] },
    message: {
      body: {
        html: {
          charset: 'UTF-8',
          data:    "<p>Your RSG PIN is #{otp}</p>",
        },
        text: {
          charset: 'UTF-8',
          data:    "Your RSG PIN is #{otp}.",
        },
      },
      subject: {
        charset: 'UTF-8',
        data:    "RSG OTP: #{otp}",
      },
    },
    source: 'info@acusense.io',
  })
end

def get_otp(email)
  pin = 6.times.map { (rand*10).to_i.to_s }.join
  n   = DB[:Users].where(email: email).update(otp: pin)
  if (n==0)
    customer_id = DB[:Customers].insert(name: 'ACME Inc.')
    DB[:Users].insert(customerId: customer_id, email: email, otp: pin)
  end
  pin
end

def post_request(event)
  meta = get_post_meta_data(event)
  body = event.merge(allowed: meta[:allowed])
  http_post(meta[:postUrl], body)      if meta[:postUrl]
  notify_slack(meta[:slackUrl], meta)  if meta[:slackUrl]
end

def notify_slack(target_url, data)
  summary = "#{(data[:allowed] ? 'Allowed' : 'Rejected')} transaction on account #{data[:investecAccountNumber]} at #{data[:merchantName]} for #{data[:currency]} #{'%.2f' % data[:amount]}."
  message = {
    "text": summary,
    "blocks": [
      {
        "type": "section",
        "block_id": "section567",
        "text": {
          "type": "mrkdwn",
          "text": ":#{(data[:allowed] ? 'simple_smile' : 'exclamation')}: #{summary}"
        },
      },
    ]
  }
  http_post(target_url, message)
end

def http_post(target_url, body)
  url          = URI(target_url)
  http         = Net::HTTP.new(url.host, url.port);
  http.use_ssl = true  if target_url.downcase.start_with?('https')
  request      = Net::HTTP::Post.new(url)
  request["Content-Type"] = "application/json"
  request.body = body.to_json
  response     = http.request(request)
  puts "Unable to POST to: #{target_url}" if response.nil? || !response.is_a?(Net::HTTPOK)
end

def get_post_meta_data(event)
  DB[:Transactions]
    .join(:Customers, id: :customerId)
    .where(investecAccountNumber: event['accountNumber'])
    .order(Sequel.desc(Sequel.lit('Transactions.id')))
    .select(
      :investecAccountNumber,
      :amount,
      :currency,
      :merchantName,
      :allowed,
      :postUrl,
      :slackUrl,
    )
    .first(1)
    .first
end

def assess_request(event)
  assessment = false
  tx         = save_transaction(event)
  assessment = assess(tx)
  DB[:Transactions].where(id: tx[:id]).update(allowed: assessment)
  {
    allow: assessment,
  }
end

def assess(tx)

  checks    = []
  decisions = []
  rules     = load_rules(tx)

  decisions << false  if rules.empty?

  rules.each do |rule|
    allow = assess_rule(tx, rule)
    decisions << allow
    checks << rule.merge(allow: allow)
  end

  save_checks(checks)

  decision =  decisions.compact

  if decision.empty?
    false
  else
    decision.reduce(true) { |result, e| result && e }
  end

end

# Each rule is an and construct: in other words, all parts of it must return true for the rule to be true.
# Outcome is allow (true), don't allow (false), no oppinion (nil)
def assess_rule(tx, rule)

  decisions = []

  rule_applies_to_this_merchant = true
  rule_applies_to_this_merchant =  (tx[:merchantName] =~ /#{rule[:ruleAppliesOnlyToMerchantRegex]}/i)  if rule[:ruleAppliesOnlyToMerchantRegex]
  rule_applies_to_this_merchant = !(tx[:merchantName] =~ /#{rule[:ruleDoesntApplyToMerchantRegex]}/i)  if rule[:ruleDoesntApplyToMerchantRegex]

  if rule_applies_to_this_merchant
    decisions << (     rule[:country] == tx[:country]                )  if rule[:country]
    decisions << (    rule[:currency] == tx[:currency]               )  if rule[:currency]
    decisions << (        tx[:amount] <= rule[:maximumAmount]        )  if rule[:maximumAmount]
    decisions << (        tx[:amount] >= rule[:minimumAmount]        )  if rule[:minimumAmount]
  end

  decisions << (  tx[:merchantName] =~ /#{rule[:allowedMerchantNameRegex]}/i )        if rule[:allowedMerchantNameRegex]
  decisions << ( (tx[:merchantName] =~ /#{rule[:blockedMerchantNameRegex]}/i).nil? )  if rule[:blockedMerchantNameRegex]
  decisions << ( rule[:allowedCategories].include?(tx[:category])  )            if rule[:allowedCategories]
  decisions << ( !rule[:blockedCategories].include?(tx[:category]) )            if rule[:blockedCategories]
  decisions << ( rule[:allowedCities].include?(tx[:city])          )            if rule[:allowedCities]
  decisions << ( rule[:blockedCities].include?(tx[:city])          )            if rule[:blockedCities]

  decisions =  decisions.compact

  if decisions.empty?
    nil
  else
    decisions.reduce(true) { |result, e| result && e }
  end

end

def load_rules(tx)
  DB[:CardRuleSets]
    .join(:RuleSets, id: :ruleSetId)
    .join(:Rules, ruleSetId: :id)
    .where(enabled: true)
    .where(deleted: false)
    .where(cardId: tx[:cardId])
    .select(
      Sequel.lit('Rules.id').as(:ruleId),
      Sequel.lit('Rules.name').as(:ruleName),
      :minimumAmount,
      :maximumAmount,
      :currency,
      :ruleAppliesOnlyToMerchantRegex,
      :ruleDoesntApplyToMerchantRegex,
      :allowedMerchantNameRegex,
      :blockedMerchantNameRegex,
      :allowedCategories,
      :blockedCategories,
      :allowedCities,
      :blockedCities,
      :country,
    )
    .all
    .map do |row|
      row[:allowedCategories] = JSON.parse(row[:allowedCategories])  if row[:allowedCategories]
      row[:blockedCategories] = JSON.parse(row[:blockedCategories])  if row[:blockedCategories]
      row[:allowedCities]     = JSON.parse(row[:allowedCities])      if row[:allowedCities]
      row[:blockedCities]     = JSON.parse(row[:blockedCities])      if row[:blockedCities]
      row
    end
    .map do |row|
      row[:transactionId]     = tx[:id]
      row[:customerId]        = tx[:customerId]
      row[:allowedCategories] = nil  if row[:allowedCategories] && row[:allowedCategories].empty?
      row[:blockedCategories] = nil  if row[:blockedCategories] && row[:blockedCategories].empty?
      row[:allowedCities]     = nil  if row[:allowedCities]     && row[:allowedCities].empty?
      row[:blockedCities]     = nil  if row[:blockedCities]     && row[:blockedCities].empty?
      row
    end
end

def save_transaction(event)
  account_number = event['accountNumber']
  amount         = (event['centsAmount']/100.0)
  card           = DB[:Cards].where(investecAccountNumber: account_number).first
  customer_id    = card[:customerId]
  card_id        = card[:id]
  tx             = {
    customerId:        customer_id,
    cardId:            card_id,
    investecAccountNumber: account_number,
    reference:         event['reference'],
    amount:            amount,
    category:          event.dig('merchant', 'category', 'key'),
    currency:          event['currencyCode'].upcase,
    merchantName:      event.dig('merchant', 'name'),
    city:              event.dig('merchant', 'city'),
    country:           event.dig('merchant', 'country', 'code').upcase,
    payload:           event.to_json,
    createdAt:         Time.now,    
  }
  tx_id          = DB[:Transactions].insert(tx)
  tx.merge(id: tx_id)
end


def save_checks(checks)
  inserts = checks.map do |check|
    check[:allowedCategories] = check[:allowedCategories].to_json
    check[:blockedCategories] = check[:blockedCategories].to_json
    check[:allowedCities]     = check[:allowedCities].to_json
    check[:blockedCities]     = check[:blockedCities].to_json
    check[:createdAt]         = Time.now
    check
  end
  DB[:Checks].multi_insert(inserts)
end