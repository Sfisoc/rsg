/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,
  cards_url:'https://5f8631f7c8a16a0016e6abcd.mockapi.io/cards'
};
