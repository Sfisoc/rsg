import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NbOAuth2LoginComponentComponent } from './nb-oauth2-login-component.component';

describe('NbOAuth2LoginComponentComponent', () => {
  let component: NbOAuth2LoginComponentComponent;
  let fixture: ComponentFixture<NbOAuth2LoginComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NbOAuth2LoginComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NbOAuth2LoginComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
