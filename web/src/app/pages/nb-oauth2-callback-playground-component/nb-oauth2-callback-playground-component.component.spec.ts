import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NbOAuth2CallbackPlaygroundComponentComponent } from './nb-oauth2-callback-playground-component.component';

describe('NbOAuth2CallbackPlaygroundComponentComponent', () => {
  let component: NbOAuth2CallbackPlaygroundComponentComponent;
  let fixture: ComponentFixture<NbOAuth2CallbackPlaygroundComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NbOAuth2CallbackPlaygroundComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NbOAuth2CallbackPlaygroundComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
