import { NgModule } from '@angular/core';
import { NbCardModule, NbLayoutModule, NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { NbOAuth2CallbackPlaygroundComponentComponent } from './nb-oauth2-callback-playground-component/nb-oauth2-callback-playground-component.component';
import { NbOAuth2LoginComponentComponent } from './nb-oauth2-login-component/nb-oauth2-login-component.component';

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    MiscellaneousModule,
    NbCardModule,
    NbLayoutModule,
  ],
  declarations: [
    PagesComponent,
    NbOAuth2CallbackPlaygroundComponentComponent,
    NbOAuth2LoginComponentComponent,
    NbOAuth2LoginComponentComponent,
    NbOAuth2CallbackPlaygroundComponentComponent,
  ],
})
export class PagesModule {
}
