import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { Card } from 'app/@core/data/models/card.model';
import { CardsTableService } from 'app/@core/mock/cards.service';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'card-crud',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent {

  cardForm: FormGroup;
  get f() { return this.cardForm.controls; }
  submitted = false;

  settings = {
    actions:
    {
      add: false
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
      },
      accountId: {
        title: 'Account No',
        type: 'string',
      },
      accountHolder: {
        title: 'Card Holder',
        type: 'string',
      },
      rules: {
        title: 'Rules',
        type: 'array',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: CardsTableService, public formBuilder: FormBuilder,
    private toastrService: NbToastrService) {
    this.loadData();
  }

  loadData() {
    const data = this.service.getData()
      .subscribe(res => {
        console.log('returned load data', res);
        this.source.load(res);
      });
  }

  ngOnInit() {
    this.cardForm = this.formBuilder.group({
      accountHolder: ['', [Validators.required]],
      accountId: ['', [Validators.required]],
      clientId: ['', [Validators.required]],
      secret: ['', [Validators.required]],
      rules: [[], [Validators.required]]
    });
  }

  onSubmit(e: Event) {
    e.preventDefault();
    this.submitted = true;
    if (this.cardForm.valid) {
      var cardJson = new Card();
      cardJson.accountHolder = this.cardForm.value.accountHolder
      cardJson.accountId = this.cardForm.value.accountId
      cardJson.clientId = this.cardForm.value.clientId
      cardJson.secret = this.cardForm.value.secret
      cardJson.rules = this.cardForm.value.rules

      this.service.add(cardJson)
        .subscribe(res => {
          console.log('Added card');
          console.log(res);
          this.toastrService.success('Added Card');
          this.loadData();
        },
          (error: any) => {
            console.log(error);
          }
        );
    }
    else {
      this.toastrService.info('All fields are required');
    }
  }

  onDeleteConfirm(event): void {

    if (window.confirm('Are you sure you want to delete?')) {
      console.log(event);

      this.service.delete(event.data.id)
        .subscribe(res => {
          console.log('Deleted card');
          console.log(res);
          this.toastrService.success('Deleted Card');
        },
          (error: any) => {
            console.log(error);
          }
        );

      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event): void {

    if (window.confirm('Are you sure you want to save edit?')) {
      console.log(event);

      var cardJson = new Card();

      cardJson.accountHolder = event.newData.accountHolder;
      cardJson.accountId = event.newData.accountId;
      cardJson.clientId = event.newData.clientId;
      cardJson.secret = event.newData.secret;

      cardJson.rules = event.newData.rules.split(',');
      cardJson.id = event.newData.id;
      console.log(cardJson);
      this.service.edit(cardJson)
        .subscribe(res => {
          console.log('Edited card');
          console.log(res);
          this.toastrService.success('Edited Card');
        },
          (error: any) => {
            console.log(error);
          }
        );

      event.confirm.resolve();
    } else {
      console.log(event + ' No');

      event.confirm.reject();
    }
  }

}
