import { Component } from '@angular/core';
import { ClpTableService } from 'app/@core/mock/clp-table.service';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'clp-table',
  templateUrl: './clp-table.component.html',
  styleUrls: ['./clp-table.component.scss'],
})
export class ClpTableComponent {

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      date: {
        title: 'Date',
        type: 'string',
      },
      vendor: {
        title: 'Vendor',
        type: 'string',
      },
      amount: {
        title: 'Amount',
        type: 'number',
      },
      rejected: {
        title: 'Rejected',
        type: 'number',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: ClpTableService) {
     const data = this.service.getData();
     this.source.load(data);

  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
