import { Observable } from 'rxjs';

export abstract class CardsTableData {
    abstract getData(): Observable<any[]>;
  }
  