import { Observable } from 'rxjs';

export interface User {
  id: string;
  username: string;
  fullname: string;
  email: string;
  roles: string[];
}

export interface Contacts {
  user: User;
  type: string;
}

export interface RecentUsers extends Contacts {
  time: number;
}

export abstract class UserData {
  abstract getUserDetail(): Observable<User>;
  // abstract getUsers(): Observable<User[]>;
  // abstract getContacts(): Observable<Contacts[]>;
  // abstract getRecentUsers(): Observable<RecentUsers[]>;
}
