import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { CardsTableData } from '../data/cards';
import { Card } from '../data/models/card.model';

@Injectable({
  providedIn: 'root'
})
export class CardsTableService extends CardsTableData {

  data = [{
    id: 1,
    cardNumber: '95485',
    cardHolder: 'Minor'
  }, {
    id: 2,
    cardNumber: '456855',
    cardHolder: 'Genie'
  }
  ];

  constructor(private http: HttpClient) {
    super();
  }

  load(): Observable<Card[]> {

    return this.http
      .get<Card[]>(environment.cards_url);
  }

  getData() {
    console.log('get data');
    return this.load();
  }

  add(item: Card) {
    return this.http.post(environment.cards_url, item);
  }

  edit(item: Card) {
    return this.http.put(environment.cards_url + '/' + item.id, item);
  }

  delete(id: number) {
    return this.http.delete(environment.cards_url + '/' + id);
  }
}
