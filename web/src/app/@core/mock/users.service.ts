import { of as observableOf, Observable, BehaviorSubject, Subscription } from 'rxjs';
import { Injectable, OnDestroy } from '@angular/core';
import { Contacts, RecentUsers, UserData, User } from '../data/users';

import { NbAuthJWTToken, NbAuthOAuth2JWTToken, NbAuthOAuth2Token, NbAuthService } from '@nebular/auth';

@Injectable()
export class UserService extends UserData implements OnDestroy {

  // private time: Date = new Date;

  // private users = {
  //   nick: { name: 'Sifiso Shabangu', picture: 'assets/images/team.png' },
  //   eva: { name: 'Eva Moor', picture: 'assets/images/eva.png' },
  //   jack: { name: 'Jack Williams', picture: 'assets/images/jack.png' },
  //   lee: { name: 'Lee Wong', picture: 'assets/images/lee.png' },
  //   alan: { name: 'Alan Thompson', picture: 'assets/images/alan.png' },
  //   kate: { name: 'Kate Martinez', picture: 'assets/images/kate.png' },
  // };
  // private types = {
  //   mobile: 'mobile',
  //   home: 'home',
  //   work: 'work',
  // };
  // private contacts: Contacts[] = [
  //   { user: this.users.nick, type: this.types.mobile },
  //   { user: this.users.eva, type: this.types.home },
  //   { user: this.users.jack, type: this.types.mobile },
  //   { user: this.users.lee, type: this.types.mobile },
  //   { user: this.users.alan, type: this.types.home },
  //   { user: this.users.kate, type: this.types.work },
  // ];
  // private recentUsers: RecentUsers[] = [
  //   { user: this.users.alan, type: this.types.home, time: this.time.setHours(21, 12) },
  //   { user: this.users.eva, type: this.types.home, time: this.time.setHours(17, 45) },
  //   { user: this.users.nick, type: this.types.mobile, time: this.time.setHours(5, 29) },
  //   { user: this.users.lee, type: this.types.mobile, time: this.time.setHours(11, 24) },
  //   { user: this.users.jack, type: this.types.mobile, time: this.time.setHours(10, 45) },
  //   { user: this.users.kate, type: this.types.work, time: this.time.setHours(9, 42) },
  //   { user: this.users.kate, type: this.types.work, time: this.time.setHours(9, 31) },
  //   { user: this.users.jack, type: this.types.mobile, time: this.time.setHours(8, 0) },
  // ];

  private _tokenChangeSubscription: Subscription;
  private _userDetailsData: User = null;
  private _userDetailsSource: BehaviorSubject<User> = new BehaviorSubject<User>(this._userDetailsData);

  constructor(private _authService: NbAuthService) {
    super();
    this._tokenChangeSubscription = this._authService.onTokenChange()
      .subscribe((token: NbAuthOAuth2Token) => {
        if (token.isValid()) {
          console.log('token', token);
          const payload = token.getValue();
          console.log('payload', payload);
          // this._userDetailsData = payload;
          this._userDetailsSource.next(this._userDetailsData);
        }
      });
  }

  ngOnDestroy(): void {
    if (this._tokenChangeSubscription)
      this._tokenChangeSubscription.unsubscribe();
  }

  getUserDetail(): Observable<any> {
    return this._userDetailsSource.asObservable();
  }

  // getUsers(): Observable<any> {
  //   return observableOf(this.users);
  // }

  // getContacts(): Observable<Contacts[]> {
  //   return observableOf(this.contacts);
  // }

  // getRecentUsers(): Observable<RecentUsers[]> {
  //   return observableOf(this.recentUsers);
  // }
}
