import { Injectable } from '@angular/core';
import { ClpTableData } from '../data/clpData';

@Injectable({
    providedIn: 'root'
  })
export class ClpTableService extends ClpTableData {

    data = [{
        id: 1,
        date: '13 November 2020',
        vendor: 'Woolies',
        amount: '4565',
        rejected: 'yes',
      },
      {
        id: 2,
        date: '14 November 2020',
        vendor: 'Makro',
        amount: '998',
        rejected: 'no',
      }
    ];

    getData(): any[] {
        return this.data
    }
}