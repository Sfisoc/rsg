// This function runs during the card transaction authorization flow.
// It has a limited execution time, so keep any code short-running.
const beforeTransaction = async (authorization) => {
    const url = "https://k7et9syyb3.execute-api.eu-west-1.amazonaws.com/Prod/assess"
    const response = await fetch(url, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(authorization)
    });
    const json = await response.json();
    let allow = json['allow']
    return allow;
};

// This function runs after a transaction.
const afterTransaction = async (transaction) => {
    const url = "https://k7et9syyb3.execute-api.eu-west-1.amazonaws.com/Prod/post"
    const response = await fetch(url, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(transaction)
    });
    await response.json();
};
