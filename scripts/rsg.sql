drop table rsg.Users;
drop table rsg.CardRuleSets;
drop table rsg.Checks;
drop table rsg.Rules;
drop table rsg.RuleSets;
drop table rsg.Transactions;
drop table rsg.Cards;
drop table rsg.Customers;


-- create table XXX (
--   id bigint NOT NULL AUTO_INCREMENT,
--   customerId bigint NOT NULL,
--   updatedAt datetime(6) NOT NULL,
--   createdAt datetime(6) NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


create table Customers (
  id bigint NOT NULL AUTO_INCREMENT,
  name varchar(128) NOT NULL,
  postUrl text,
  slackUrl text,
  updatedAt datetime(6) NOT NULL,
  createdAt datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_customers_on_name` (`name`),
  KEY `index_customers_on_updated_at` (`updatedAt`),
  KEY `index_customers_on_created_at` (`createdAt`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- insert into Customers (name, updatedAt, createdAt) values ('Siemens', now(), now());
-- insert into Customers (name, updatedAt, createdAt) values ('Old Mutual', now(), now());
-- insert into Customers (name, updatedAt, createdAt) values ('Investec', now(), now());

create table Users (
  id bigint NOT NULL AUTO_INCREMENT,
  customerId bigint NOT NULL,
  email varchar(255) NOT NULL,

  -- oAuth fields
  provider varchar(32) DEFAULT NULL,
  uid varchar(255) DEFAULT NULL,
  encrypted_password varchar(255) NOT NULL DEFAULT '',
  remember_created_at datetime(6) DEFAULT NULL,
  sign_in_count int NOT NULL DEFAULT '0',
  current_sign_in_at datetime DEFAULT NULL,
  last_sign_in_at datetime DEFAULT NULL,
  current_sign_in_ip varchar(255) DEFAULT NULL,
  last_sign_in_ip varchar(255) DEFAULT NULL,

  updatedAt datetime(6) NOT NULL,
  createdAt datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_users_on_customer_id` (`customerId`),
  KEY `index_users_on_name` (`email`),
  KEY `index_users_on_uid` (`uid`),
  KEY `index_users_on_updated_at` (`updatedAt`),
  KEY `index_users_on_created_at` (`createdAt`),
  FOREIGN KEY (customerId) REFERENCES Customers(id)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- insert into Users (customerId, email, updatedAt, createdAt) values (9, 'renen@121.co.za', now(), now());
-- insert into Users (customerId, email, updatedAt, createdAt) values (7, 'cshabangus@gmail.com', now(), now());

create table Cards (
  id bigint NOT NULL AUTO_INCREMENT,
  customerId bigint NOT NULL,
  investecClientId varchar(64) NOT NULL,
  investecSecret varchar(64) NOT NULL,
  investecAccountNumber varchar(64) NOT NULL,
  cardHolderName varchar(128) NOT NULL,
  updatedAt datetime(6) NOT NULL,
  createdAt datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_cards_on_investec_customer_id` (`customerId`),
  KEY `index_cards_on_investec_client_id` (`investecClientId`),
  KEY `index_cards_on_investec_secret` (`investecSecret`),
  KEY `index_cards_on_account_id` (`investecAccountNumber`),
  KEY `index_cards_on_updated_at` (`updatedAt`),
  KEY `index_cards_on_created_at` (`createdAt`),
  FOREIGN KEY (customerId) REFERENCES Customers(id)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- insert into Cards (customerId, investecClientId, investecSecret, investecAccountNumber, cardHolderName, updatedAt, createdAt) values (9, 'xxxx', 'yyyy', '10010159833', 'SE Watermeyer', now(), now());

create table Transactions (
  id bigint NOT NULL AUTO_INCREMENT,

  customerId bigint NOT NULL,
  cardId bigint NOT NULL,

  investecAccountNumber varchar(64) NOT NULL,
  reference         varchar(255) NOT NULL,
  amount            decimal(8,2) DEFAULT NULL,
  currency          varchar(3) NOT NULL DEFAULT 'ZA',

  merchantName      varchar(255) NOT NULL DEFAULT '',
  category          varchar(255) NOT NULL DEFAULT '',

  city              varchar(255) NOT NULL DEFAULT 'ZA',
  country           varchar(3) NOT NULL DEFAULT 'ZA',

  payload           text,

  allowed           TINYINT(1) DEFAULT 1,

  createdAt datetime(6) NOT NULL,

  PRIMARY KEY (`id`),
  KEY `index_transactions_on_customer_id` (`customerId`),
  KEY `index_transactions_on_card_id` (`investecAccountNumber`),
  KEY `index_transactions_on_created_at` (`createdAt`),
  FOREIGN KEY (customerId) REFERENCES Customers(id),
  FOREIGN KEY (investecAccountNumber) REFERENCES Cards(investecAccountNumber)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

create table RuleSets (
  id bigint NOT NULL AUTO_INCREMENT,
  customerId bigint NOT NULL,
  name varchar(128) NOT NULL,
  deleted TINYINT(1) DEFAULT 0,
  updatedAt datetime(6) NOT NULL,
  createdAt datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_rule_sets_on_customer_id` (`customerId`),
  KEY `index_rule_sets_on_name` (`name`),
  KEY `index_rule_sets_on_updated_at` (`updatedAt`),
  KEY `index_rule_sets_on_created_at` (`createdAt`),
  FOREIGN KEY (customerId) REFERENCES Customers(id)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- insert into RuleSets (customerId, name, deleted, updatedAt, createdAt) values (9, 'Pocket Money Rules', 0, now(), now());

create table Rules (
  id bigint NOT NULL AUTO_INCREMENT,
  customerId bigint NOT NULL,
  ruleSetId bigint NOT NULL,
  name varchar(128) NOT NULL,

  minimumAmount     decimal(8,2),
  maximumAmount     decimal(8,2),
  currency          varchar(3),
  ruleAppliesOnlyToMerchantRegex text,
  ruleDoesntApplyToMerchantRegex text,
  allowedMerchantNameRegex text,
  blockedMerchantNameRegex text,
  allowedCategories text,
  blockedCategories text,
  allowedCities     text,
  blockedCities     text,
  country           varchar(3),

  updatedAt datetime(6) NOT NULL,
  createdAt datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_rules_on_customer_id` (`customerId`),
  KEY `index_rules_on_name` (`name`),
  KEY `index_rules_on_updated_at` (`updatedAt`),
  KEY `index_rules_on_created_at` (`createdAt`),
  FOREIGN KEY (customerId) REFERENCES Customers(id),
  FOREIGN KEY (ruleSetId) REFERENCES RuleSets(id)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- insert into Rules (customerId, ruleSetId, name, ruleAppliesOnlyToMerchantRegex, maximumAmount, updatedAt, createdAt) values (9, 7, 'Woolworths', 'Wool', 500.00, now(), now());
-- insert into Rules (customerId, ruleSetId, name, ruleAppliesOnlyToMerchantRegex, maximumAmount, updatedAt, createdAt) values (9, 7, 'Pick n Pay', 'Pick', 300.00, now(), now());
-- insert into Rules (customerId, ruleSetId, name, ruleDoesntApplyToMerchantRegex, maximumAmount, updatedAt, createdAt) values (9, 7, 'Max for non-food retailers', 'Pick|Wool', 150.00, now(), now());
-- insert into Rules (customerId, ruleSetId, name, blockedMerchantNameRegex, updatedAt, createdAt) values (9, 7, 'Mavericks', 'Mavericks', now(), now());
-- insert into Rules (customerId, ruleSetId, name, blockedCategories, updatedAt, createdAt) values (9, 7, 'No Booze', '["liquor", "booze"]', now(), now());


create table CardRuleSets (
  id bigint NOT NULL AUTO_INCREMENT,
  customerId bigint NOT NULL,
  cardId bigint NOT NULL,
  ruleSetId bigint NOT NULL,
  enabled TINYINT(1) DEFAULT 1,
  updatedAt datetime(6) NOT NULL,
  createdAt datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_card_rule_sets_on_customer_id` (`customerId`),
  KEY `index_card_rule_sets_on_card_id` (`cardId`),
  KEY `index_card_rule_sets_on_rule_set_id` (`ruleSetId`),
  KEY `index_card_rule_sets_on_updated_at` (`updatedAt`),
  KEY `index_card_rule_sets_on_created_at` (`createdAt`),
  FOREIGN KEY (customerId) REFERENCES Customers(id),
  FOREIGN KEY (cardId) REFERENCES Cards(id),
  FOREIGN KEY (ruleSetId) REFERENCES RuleSets(id)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- insert into CardRuleSets (customerId, cardId, ruleSetId, enabled, updatedAt, createdAt) values (9, 7, 7, 1, now(), now());

create table Checks (
  id bigint NOT NULL AUTO_INCREMENT,
  customerId bigint NOT NULL,

  transactionId     bigint NOT NULL,
  ruleId            bigint NOT NULL,
  ruleName          varchar(128) NOT NULL,

  minimumAmount     decimal(8,2),
  maximumAmount     decimal(8,2),
  currency          varchar(3),
  ruleAppliesOnlyToMerchantRegex text,
  ruleDoesntApplyToMerchantRegex text,
  allowedMerchantNameRegex text,
  blockedMerchantNameRegex text,
  allowedCategories text,
  blockedCategories text,
  allowedCities     text,
  blockedCities     text,
  country           varchar(3),

  allow TINYINT(1) DEFAULT 0,

  createdAt datetime(6) NOT NULL,

  PRIMARY KEY (`id`),
  KEY `index_checks_on_customer_id` (`customerId`),
  KEY `index_checks_on_transaction_id` (`transactionId`),
  KEY `index_checks_on_rule_id` (`ruleId`),
  KEY `index_checks_on_created_at` (`createdAt`),
  FOREIGN KEY (customerId) REFERENCES Customers(id),
  FOREIGN KEY (transactionId) REFERENCES Transactions(id),
  FOREIGN KEY (ruleId) REFERENCES Rules(id)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

