package com.rsg.rsgServer.repos;

import com.rsg.rsgServer.RsgServerApplication;
import com.rsg.rsgServer.models.Customers;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;
import org.junit.Test;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RsgServerApplication.class)
public class CustomersRepo {

    @Autowired( required = true )
    private CustomersRepository userRepository;

    @Test
    public void testGetCustomers()
    {
        Optional<Customers> byId = userRepository.findById(1);

        if(byId.isPresent()) {
            Customers users = byId.get();
            assertTrue(users != null);
        }
        else
        {
            assertTrue(byId.isPresent());
        }
    }

}
