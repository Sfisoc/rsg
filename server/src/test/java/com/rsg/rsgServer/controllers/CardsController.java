package com.rsg.rsgServer.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rsg.rsgServer.RsgServerApplication;
import com.rsg.rsgServer.models.Cards;
import com.rsg.rsgServer.models.Customers;
import com.rsg.rsgServer.repos.CardsRepository;
import com.rsg.rsgServer.repos.CustomersRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.smartcardio.Card;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RsgServerApplication.class)
public class CardsController {
    MockMvc mvc;

    @Autowired( required = true )
    private CardsRepository userRepository;

    ObjectMapper mapper = new ObjectMapper();



    @InjectMocks
    private CardController categoryController;

    @Before
    public void setup() {
        categoryController = new CardController(userRepository);
        mvc = MockMvcBuilders.standaloneSetup(categoryController).build();
    }

    @Test
    public void testGetCardsById() throws Exception {
        ResultActions resultActions = this.mvc.perform(get("/cards"))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());

    }

    @Test
    public void testAddCards() throws Exception {

        Cards input  = new Cards();

        input.setCardHolderName("Test test Add cards");
        input.setInvestecAccountNumber("WEEED456");
        input.setInvestecClientId("WEEED456");
        input.setInvestecSecret("WEEED456");

        Customers addition =  new Customers();

        addition.setId(9);

       input.setCustomerId(addition);


        String json = mapper.writeValueAsString(input);

        ResultActions resultActions = this.mvc.perform(post("/cards").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());

    }

    @Test
    public void testUpdateCustomers() throws Exception {

        Cards input  = new Cards();

        input.setId(9);
        input.setCardHolderName("Test test Add cards");
        input.setInvestecAccountNumber("REDDD12 Updated");
        input.setInvestecClientId("WEEED456 Updated");
        input.setInvestecSecret("WEEED456 Updated");

        Customers addition =  new Customers();

        addition.setId(7);

        input.setCustomerId(addition);


        String json = mapper.writeValueAsString(input);


        ResultActions resultActions = this.mvc.perform(put("/cards/9").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());
    }


}
