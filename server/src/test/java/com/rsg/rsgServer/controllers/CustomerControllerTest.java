package com.rsg.rsgServer.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rsg.rsgServer.RsgServerApplication;
import com.rsg.rsgServer.models.Customers;
import com.rsg.rsgServer.repos.CustomersRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RsgServerApplication.class)
public class CustomerControllerTest {

    MockMvc mvc;

    @Autowired( required = true )
    private CustomersRepository userRepository;

    ObjectMapper mapper = new ObjectMapper();



    @InjectMocks
    private CustomerController categoryController;

    @Before
    public void setup() {
        categoryController = new CustomerController(userRepository);
        mvc = MockMvcBuilders.standaloneSetup(categoryController).build();
    }

    @Test
    public void testGetCustomersById() throws Exception {
        ResultActions resultActions = this.mvc.perform(get("/customers"))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());

    }

    @Test
    public void testAddCustomers() throws Exception {

        Customers input  = new Customers();

        input.setName("Test testAddCustomers");

        String json = mapper.writeValueAsString(input);

        ResultActions resultActions = this.mvc.perform(post("/customers").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());

    }

    @Test
    public void testUpdateCustomers() throws Exception {

        Customers input  = new Customers();

        input.setId(10);
        input.setName("Test Updated ");

        String json = mapper.writeValueAsString(input);

        ResultActions resultActions = this.mvc.perform(put("/customers/10").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());

    }

}
