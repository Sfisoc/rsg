package com.rsg.rsgServer.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rsg.rsgServer.Documents.PasswordChangeInput;
import com.rsg.rsgServer.Documents.UserLoginInput;
import com.rsg.rsgServer.RsgServerApplication;
import com.rsg.rsgServer.models.Customers;
import com.rsg.rsgServer.models.Users;
import com.rsg.rsgServer.repos.UsersRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RsgServerApplication.class)
public class UsersControllerTest {

    MockMvc mvc;

    @Autowired( required = true )
    private UsersRepository userRepository;

    ObjectMapper mapper = new ObjectMapper();

    @InjectMocks
    private UsersController categoryController;

    @Before
    public void setup() {
        categoryController = new UsersController(userRepository);
        mvc = MockMvcBuilders.standaloneSetup(categoryController).build();
    }

    @Test
    public void testGetAll() throws Exception {
        ResultActions resultActions = this.mvc.perform(get("/users"))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());
    }

    @Test
    public void testAdd() throws Exception {

        Users input  = new Users();

        input.setEmail("Test@gmail");
        input.setEncrypted_password("pass");
        input.setProvider("Unit Test");
        Customers customers = new Customers();
        customers.setId(7);
        input.setCustomerId(customers);

        String json = mapper.writeValueAsString(input);

        ResultActions resultActions = this.mvc.perform(post("/users").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());

    }

    @Test
    public void testUpdate() throws Exception {
        Users input  = new Users();

        input.setId(9);
        input.setEmail("Test@gmailUpdate");
        input.setEncrypted_password("pass");
        input.setProvider("Unit Test");
        Customers customers = new Customers();
        customers.setId(7);
        input.setCustomerId(customers);


        String json = mapper.writeValueAsString(input);

        ResultActions resultActions = this.mvc.perform(put("/users/9").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());

    }

    @Test
    public void testLogin() throws Exception {

        UserLoginInput test = new UserLoginInput();

        test.setPassword("pass");
        test.setUserIdentity("Test@gmailUpdate");

        String json = mapper.writeValueAsString(test);

        ResultActions resultActions = this.mvc.perform(put("/users/login").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());
    }

    @Test
    public void testPasswordChange() throws Exception {

        PasswordChangeInput test = new PasswordChangeInput();

        test.setOldPassword("pass");
        test.setNewPassword("pass1");

        String json = mapper.writeValueAsString(test);

        ResultActions resultActions = this.mvc.perform(put("/users/9/pwd/update").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());
    }


}
