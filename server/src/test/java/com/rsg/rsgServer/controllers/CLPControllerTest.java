package com.rsg.rsgServer.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rsg.rsgServer.Documents.ClpInput;
import com.rsg.rsgServer.RsgServerApplication;
import com.rsg.rsgServer.models.Cards;
import com.rsg.rsgServer.models.Customers;
import com.rsg.rsgServer.repos.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RsgServerApplication.class)
public class CLPControllerTest {
    MockMvc mvc;

    @Autowired( required = true )
    private CardsRepository userRepository;

    @Autowired( required = true )
    private CardsRepository cards;

    @Autowired( required = true )
    private CardRuleSetsRepository cardRules;

    @Autowired( required = true )
    private ChecksRepository checks;

    @Autowired( required = true )
    private RulesRepository rules;

    @Autowired( required = true )
    private RulesSetsRepository ruleSets;

    @Autowired( required = true )
    private TransactionsRepository transactions;

    ObjectMapper mapper = new ObjectMapper();

    @InjectMocks
    private CLPController categoryController;

    @Before
    public void setup() {
        categoryController = new CLPController(transactions,ruleSets,rules,checks,cardRules,cards);
        mvc = MockMvcBuilders.standaloneSetup(categoryController).build();
    }

    @Test
    public void testClpGet() throws Exception {

        ClpInput input  = new ClpInput();
        input.setCustomerId(9);
        input.setUserId(9);

        String json = mapper.writeValueAsString(input);

        ResultActions resultActions = this.mvc.perform(post("/clp/data").content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());//

        System.out.println(resultActions.toString());

    }

}
