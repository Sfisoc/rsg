package com.rsg.rsgServer.controllers;

import com.rsg.rsgServer.Documents.LoginOutput;
import com.rsg.rsgServer.Documents.PasswordChangeInput;
import com.rsg.rsgServer.Documents.UserLoginInput;
import com.rsg.rsgServer.execeptions.RsgNotFoundException;
import com.rsg.rsgServer.models.Users;
import com.rsg.rsgServer.repos.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
@CrossOrigin
public class UsersController {

    @Autowired
    private UsersRepository repo;



    public UsersController(UsersRepository repository)
    {
        this.repo = repository;
    }

    public UsersController()
    {

    }

    @PostMapping(path="/users") // Map ONLY POST Requests
    public @ResponseBody
    Users add (@RequestBody Users input, HttpServletRequest request) {
        UUID uuid = UUID.randomUUID();

        input.setUpdatedAt(LocalDateTime.now());
        input.setCreatedAt(LocalDateTime.now());
        input.setRemember_created_at(LocalDateTime.now());
        input.setSign_in_count(0);
        input.setCurrent_sign_in_ip(request.getRemoteAddr());
        input.setCurrent_sign_in_ip(request.getRemoteAddr());
        input.setUid(uuid.toString());

        Users save = repo.save(input);

        return save;
    }

    @GetMapping(path="/users")
    public @ResponseBody Iterable<Users> getAll() {

        return repo.findAll();
    }

    @GetMapping(path="/users/{id}")
    public @ResponseBody Users get(@PathVariable Long id) {

        return repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));
    }

    @PutMapping(path="/users/{id}")
    public @ResponseBody
    ResponseEntity update(@PathVariable Long id, @RequestBody  Users input) {

        Users item = repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));

        item.setEmail(input.getEmail());
        item.setProvider(input.getProvider());
        item.setCustomerId(input.getCustomerId());

        item.setUpdatedAt(LocalDateTime.now());

        final  Users output = repo.save(item);

        return ResponseEntity.ok(output);
    }

    @PutMapping(path="/users/{id}/pwd/update")
    public @ResponseBody
    ResponseEntity updatePWD(@PathVariable Long id, @RequestBody PasswordChangeInput input) {

        Users item = repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));

       if(item.getEncrypted_password().equals(input.getOldPassword()))
       {
           item.setEncrypted_password(input.getNewPassword());

           final  Users output = repo.save(item);

           return ResponseEntity.ok(output);
       }
       else
       {
           return ResponseEntity.ok(false);
       }

    }


    @PostMapping(path="/users/login")
    public @ResponseBody
    ResponseEntity login(@RequestBody UserLoginInput input, HttpServletRequest request) {

        Iterable<Users> item = repo.findAll();

        Stream<Users> targetStream = StreamSupport.stream(item.spliterator(), false);
        final Optional<Users> first = targetStream.filter(x -> x.getEmail().equals(input.getUserIdentity()) && x.getEncrypted_password().equals(input.getPassword())).findFirst();

        if(first.isPresent())
        {
            Users users = first.get();

            users.setCurrent_sign_in_ip(request.getRemoteAddr());
            users.setSign_in_count(users.getSign_in_count()+1);
            users.setCurrent_sign_in_at(LocalDateTime.now());
            LoginOutput tmp = new LoginOutput();
            tmp.setLogin(true);
            tmp.setCustomerId(users.getCustomerId().getId());
            tmp.setUserId(users.getId());

            repo.save(users);

            return ResponseEntity.ok(tmp);
        }
        else
        {
            return ResponseEntity.ok(false);
        }

    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Long id) {
        repo.deleteById(id.intValue());
    }
}
