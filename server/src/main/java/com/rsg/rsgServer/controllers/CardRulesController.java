package com.rsg.rsgServer.controllers;

import com.rsg.rsgServer.execeptions.RsgNotFoundException;
import com.rsg.rsgServer.models.CardRuleSets;
import com.rsg.rsgServer.models.Customers;
import com.rsg.rsgServer.models.RuleSets;
import com.rsg.rsgServer.repos.CardRuleSetsRepository;
import com.rsg.rsgServer.repos.RulesSetsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@CrossOrigin
public class CardRulesController {
    @Autowired
    private CardRuleSetsRepository repo;


    public CardRulesController(CardRuleSetsRepository repository)
    {
        this.repo = repository;
    }

    public CardRulesController()
    {

    }

    @PostMapping(path="/cardRules") // Map ONLY POST Requests
    public @ResponseBody
    CardRuleSets add (@RequestBody CardRuleSets input) {

        input.setUpdatedAt(LocalDateTime.now());
        input.setCreatedAt(LocalDateTime.now());

        CardRuleSets save = repo.save(input);

        return save;
    }

    @GetMapping(path="/cardRules")
    public @ResponseBody Iterable<CardRuleSets> getAll() {

        return repo.findAll();
    }

    @GetMapping(path="/cardRules/{id}")
    public @ResponseBody CardRuleSets get(@PathVariable Long id) {

        return repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));
    }

    @PutMapping(path="/cardRules/{id}")
    public @ResponseBody
    ResponseEntity update(@PathVariable Long id, @RequestBody  CardRuleSets input) {

        CardRuleSets item = repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));

        item.setCardId(input.getCardId());
        item.setEnabled(input.isEnabled());
        item.setCustomerId(input.getCustomerId());
        item.setRuleSetId(input.getRuleSetId());

        item.setUpdatedAt(LocalDateTime.now());

        final  CardRuleSets output = repo.save(item);

        return ResponseEntity.ok(output);
    }

    @DeleteMapping("/cardRules/{id}")
    public void delete(@PathVariable Long id) {
        repo.deleteById(id.intValue());
    }
}
