package com.rsg.rsgServer.controllers;

import com.rsg.rsgServer.execeptions.RsgNotFoundException;
import com.rsg.rsgServer.models.Cards;
import com.rsg.rsgServer.models.Customers;
import com.rsg.rsgServer.models.RuleSets;
import com.rsg.rsgServer.repos.CardsRepository;
import com.rsg.rsgServer.repos.RulesSetsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@CrossOrigin
public class RuleSetsController {

    @Autowired
    private RulesSetsRepository repo;


    public RuleSetsController(RulesSetsRepository repository)
    {
        this.repo = repository;
    }

    public RuleSetsController()
    {

    }

    @PostMapping(path="/ruleSets") // Map ONLY POST Requests
    public @ResponseBody
    RuleSets add (@RequestBody RuleSets input) {

        input.setUpdatedAt(LocalDateTime.now());
        input.setCreatedAt(LocalDateTime.now());

        RuleSets save = repo.save(input);

        return save;
    }

    @GetMapping(path="/ruleSets")
    public @ResponseBody Iterable<RuleSets> getAll() {

        return repo.findAll();
    }

    @GetMapping(path="/ruleSets/{id}")
    public @ResponseBody RuleSets get(@PathVariable Long id) {

        return repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));
    }

    @PutMapping(path="/ruleSets/{id}")
    public @ResponseBody
    ResponseEntity update(@PathVariable Long id, @RequestBody  RuleSets input) {

        RuleSets item = repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));

        item.setName(input.getName());
        item.setDeleted(input.getDeleted());

        Customers customers = new Customers();
        customers.setId(input.getCustomerId().getId());

        item.setCustomerId(customers);

        item.setUpdatedAt(LocalDateTime.now());

        final  RuleSets output = repo.save(item);

        return ResponseEntity.ok(output);
    }

    @DeleteMapping("/ruleSets/{id}")
    public void delete(@PathVariable Long id) {
        repo.deleteById(id.intValue());
    }
}
