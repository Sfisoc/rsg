package com.rsg.rsgServer.controllers;

import com.rsg.rsgServer.execeptions.RsgNotFoundException;
import com.rsg.rsgServer.models.CardRuleSets;
import com.rsg.rsgServer.models.Checks;
import com.rsg.rsgServer.repos.CardRuleSetsRepository;
import com.rsg.rsgServer.repos.ChecksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@CrossOrigin
public class ChecksController {

    @Autowired
    private ChecksRepository repo;


    public ChecksController(ChecksRepository repository)
    {
        this.repo = repository;
    }

    public ChecksController()
    {

    }

    @PostMapping(path="/checks") // Map ONLY POST Requests
    public @ResponseBody
    Checks add (@RequestBody Checks input) {

        input.setCreatedAt(LocalDateTime.now());

        Checks save = repo.save(input);

        return save;
    }


    @GetMapping(path="/checks")
    public @ResponseBody Iterable<Checks> getAll() {

        return repo.findAll();
    }

    @GetMapping(path="/checks/{id}")
    public @ResponseBody Checks get(@PathVariable Long id) {

        return repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));
    }

    @PutMapping(path="/checks/{id}")
    public @ResponseBody
    ResponseEntity update(@PathVariable Long id, @RequestBody  Checks input) {

        Checks item = repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));

        item.setAllow(input.isAllow());
        item.setAllowedCategories(input.getAllowedCategories());
        item.setAllowedCities(input.getAllowedCities());
        item.setBlockedCategories(input.getBlockedCategories());
        item.setBlockedCities(input.getBlockedCities());
        item.setBlockedMerchantNameRegex(input.getBlockedMerchantNameRegex());
        item.setCountry(input.getCountry());
        item.setCurrency(input.getCurrency());
        item.setCustomerId(input.getCustomerId());
        item.setMaximumAmount(input.getMaximumAmount());
        item.setMinimumAmount(input.getMinimumAmount());
        item.setRuleAppliesOnlyToMerchantRegex(input.getRuleAppliesOnlyToMerchantRegex());
        item.setRuleDoesntApplyToMerchantRegex(input.getRuleDoesntApplyToMerchantRegex());
        item.setRuleId(input.getRuleId());
        item.setRuleName(input.getRuleName());
        item.setTransactionId(input.getTransactionId());

        final  Checks output = repo.save(item);

        return ResponseEntity.ok(output);
    }

    @DeleteMapping("/checks/{id}")
    public void delete(@PathVariable Long id) {
        repo.deleteById(id.intValue());
    }
}
