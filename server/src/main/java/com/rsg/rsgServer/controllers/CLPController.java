package com.rsg.rsgServer.controllers;

import com.rsg.rsgServer.Documents.ClpInput;
import com.rsg.rsgServer.Documents.ClpOut;
import com.rsg.rsgServer.models.*;
import com.rsg.rsgServer.repos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
@CrossOrigin
public class CLPController {

    @Autowired
    private CardsRepository cards;

    @Autowired
    private CardRuleSetsRepository cardRules;

    @Autowired
    private ChecksRepository checks;

    @Autowired
    private RulesRepository rules;

    @Autowired
    private RulesSetsRepository ruleSets;

    @Autowired
    private TransactionsRepository transactions;
//
//    @Autowired
//    private UsersRepository users;

    public CLPController()
    {

    }

    public CLPController(TransactionsRepository repo1,RulesSetsRepository repo2,RulesRepository repo3,
    ChecksRepository repo4,CardRuleSetsRepository repo5,CardsRepository repo6)
    {
        this.transactions = repo1;
        this.ruleSets = repo2;
        this.rules = repo3;
        this.checks = repo4;
        this.cards = repo6;
        this.cardRules = repo5;

    }

    @PostMapping(path="/clp/data") // Map ONLY POST Requests
    public @ResponseBody
    ResponseEntity getData (@RequestBody ClpInput input) {

        Iterable<CardRuleSets> cardRulesAll = this.cardRules.findAll();
        Iterable<Cards> cards = this.cards.findAll();
        Iterable<Checks> checksAll = this.checks.findAll();
        Iterable<Rules> rulesAll = this.rules.findAll();
        Iterable<RuleSets> ruleSetsAll = this.ruleSets.findAll();
        Iterable<Transactions> transactionsAll = this.transactions.findAll();
//        Iterable<Users> usersAll = this.users.findAll();

        ClpOut data = new ClpOut();

        Stream<CardRuleSets> targetStream = StreamSupport.stream(cardRulesAll.spliterator(), false);
        List<CardRuleSets> listCardRuleSet = targetStream.filter(x -> x.getCustomerId().getId().equals(input.getCustomerId())).collect(Collectors.toList());
        data.setCardRuleSets(listCardRuleSet);

        Stream<Cards> targetStream1 = StreamSupport.stream(cards.spliterator(), false);
        List<Cards> collect1 = targetStream1.filter(x -> x.getCustomerId().getId().equals(input.getCustomerId())).collect(Collectors.toList());
        data.setCards(collect1);

        Stream<Checks> targetStream2 = StreamSupport.stream(checksAll.spliterator(), false);
        List<Checks> collect2 = targetStream2.filter(x -> x.getCustomerId().getId().equals(input.getCustomerId())).collect(Collectors.toList());
        data.setChecks(collect2);

        Stream<Rules> targetStream3 = StreamSupport.stream(rulesAll.spliterator(), false);
        List<Rules> collect3 = targetStream3.filter(x -> x.getCustomerId().getId().equals(input.getCustomerId())).collect(Collectors.toList());
        data.setRules(collect3);

        Stream<RuleSets> targetStream4 = StreamSupport.stream(ruleSetsAll.spliterator(), false);
        List<RuleSets> collect4 = targetStream4.filter(x -> x.getCustomerId().getId().equals(input.getCustomerId())).collect(Collectors.toList());
        data.setRuleSets(collect4);

        Stream<Transactions> targetStream5 = StreamSupport.stream(transactionsAll.spliterator(), false);
        List<Transactions> collect5 = targetStream5.filter(x -> x.getCustomerId().getId().equals(input.getCustomerId())).collect(Collectors.toList());
        data.setTransactions(collect5);

        return ResponseEntity.ok(data);
    }
}
