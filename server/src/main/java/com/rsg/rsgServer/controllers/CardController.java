package com.rsg.rsgServer.controllers;

import com.rsg.rsgServer.execeptions.RsgNotFoundException;
import com.rsg.rsgServer.models.Cards;
import com.rsg.rsgServer.models.Customers;
import com.rsg.rsgServer.repos.CardsRepository;
import com.rsg.rsgServer.repos.CustomersRepository;
import com.rsg.rsgServer.repos.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@CrossOrigin
public class CardController {

    @Autowired
    private CardsRepository repo;

    public CardController(CardsRepository repository)
    {
        this.repo = repository;
    }

    public CardController()
    {

    }

    @PostMapping(path="/cards") // Map ONLY POST Requests
    public @ResponseBody
    Cards add (@RequestBody Cards input) {

        input.setUpdatedAt(LocalDateTime.now());
        input.setCreatedAt(LocalDateTime.now());

        Cards save = repo.save(input);

        return save;
    }

    @GetMapping(path="/cards")
    public @ResponseBody Iterable<Cards> getAll() {

        return repo.findAll();
    }

    @GetMapping(path="/cards/{id}")
    public @ResponseBody Cards get(@PathVariable Long id) {

        return repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));
    }

    @PutMapping(path="/cards/{id}")
    public @ResponseBody
    ResponseEntity update(@PathVariable Long id, @RequestBody  Cards input) {

        Cards item = repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));

        item.setInvestecClientId(input.getInvestecClientId());
        item.setInvestecSecret(input.getInvestecSecret());
        item.setInvestecAccountNumber(input.getInvestecAccountNumber());
        item.setCardHolderName(input.getCardHolderName());

        Customers customers = new Customers();
        customers.setId(input.getCustomerId().getId());

        item.setCustomerId(customers);

        item.setUpdatedAt(LocalDateTime.now());

        final  Cards output = repo.save(item);

        return ResponseEntity.ok(output);
    }

    @DeleteMapping("/cards/{id}")
    public void delete(@PathVariable Long id) {
        repo.deleteById(id.intValue());
    }
}
