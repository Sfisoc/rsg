package com.rsg.rsgServer.controllers;

import com.rsg.rsgServer.execeptions.RsgNotFoundException;
import com.rsg.rsgServer.models.Customers;
import com.rsg.rsgServer.models.Users;
import com.rsg.rsgServer.repos.CardsRepository;
import com.rsg.rsgServer.repos.CustomersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@CrossOrigin
public class CustomerController {

    @Autowired
    private CustomersRepository repo;

    public CustomerController(CustomersRepository repository)
    {
        this.repo = repository;
    }

    public CustomerController()
    {
    }

    @PostMapping(path="/customers") // Map ONLY POST Requests
    public @ResponseBody
    ResponseEntity add (@RequestBody  Customers input) {

        input.setUpdatedAt(LocalDateTime.now());
        input.setCreatedAt(LocalDateTime.now());

        Customers save = repo.save(input);

        return ResponseEntity.ok(save);
    }

    @GetMapping(path="/customers")
    public @ResponseBody ResponseEntity getAll() {

        final Iterable<Customers> all = repo.findAll();

        return ResponseEntity.ok(all);
    }

    @GetMapping(path="/customers/{id}")
    public @ResponseBody ResponseEntity get(@PathVariable Long id) {

        return ResponseEntity.ok(repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id)));
    }

    @PutMapping(path="/customers/{id}")
    public @ResponseBody ResponseEntity update(@PathVariable Long id, @RequestBody  Customers input) {

        Customers customers = repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));

        customers.setName(input.getName());
        customers.setPostUrl(input.getPostUrl());
        customers.setSlackUrl(input.getSlackUrl());

        customers.setUpdatedAt(LocalDateTime.now());

        final  Customers output = repo.save(customers);

        return ResponseEntity.ok(output);
    }

    @DeleteMapping("/customers/{id}")
    public void delete(@PathVariable Long id) {
        repo.deleteById(id.intValue());
    }

}