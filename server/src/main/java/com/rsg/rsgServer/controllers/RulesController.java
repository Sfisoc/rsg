package com.rsg.rsgServer.controllers;

import com.rsg.rsgServer.execeptions.RsgNotFoundException;
import com.rsg.rsgServer.models.Rules;
import com.rsg.rsgServer.repos.RulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@CrossOrigin
public class RulesController {

    @Autowired
    private RulesRepository repo;



    public RulesController(RulesRepository repository)
    {
        this.repo = repository;
    }

    public RulesController()
    {

    }

    @PostMapping(path="/rules") // Map ONLY POST Requests
    public @ResponseBody
    Rules add (@RequestBody Rules input) {

        input.setUpdatedAt(LocalDateTime.now());
        input.setCreatedAt(LocalDateTime.now());

        Rules save = repo.save(input);

        return save;
    }

    @GetMapping(path="/rules")
    public @ResponseBody Iterable<Rules> getAll() {

        return repo.findAll();
    }

    @GetMapping(path="/rules/{id}")
    public @ResponseBody Rules get(@PathVariable Long id) {

        return repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));
    }

    @PutMapping(path="/rules/{id}")
    public @ResponseBody
    ResponseEntity update(@PathVariable Long id, @RequestBody  Rules input) {

        Rules item = repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));

        item.setName(input.getName());
        item.setCurrency(input.getCurrency());
        item.setRuleAppliesOnlyToMerchantRegex(input.getRuleAppliesOnlyToMerchantRegex());
        item.setRuleDoesntApplyToMerchantRegex(input.getRuleDoesntApplyToMerchantRegex());
        item.setAllowedCategories(input.getAllowedCategories());
        item.setBlockedMerchantNameRegex(input.getBlockedMerchantNameRegex());
        item.setAllowedCategories(input.getAllowedCategories());
        input.setAllowedCities(input.getAllowedCities());
        input.setBlockedCities(input.getBlockedCities());
        input.setCountry(input.getCountry());
        item.setMinimumAmount(input.getMinimumAmount());
        item.setMaximumAmount(input.getMaximumAmount());
        item.setCustomerId(input.getCustomerId());
        item.setRuleSetId(input.getRuleSetId());

        item.setUpdatedAt(LocalDateTime.now());

        final  Rules output = repo.save(item);

        return ResponseEntity.ok(output);
    }

    @DeleteMapping("/rules/{id}")
    public void delete(@PathVariable Long id) {
        repo.deleteById(id.intValue());
    }
}

