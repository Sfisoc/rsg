package com.rsg.rsgServer.controllers;

import com.rsg.rsgServer.execeptions.RsgNotFoundException;
import com.rsg.rsgServer.models.Checks;
import com.rsg.rsgServer.models.Transactions;
import com.rsg.rsgServer.repos.ChecksRepository;
import com.rsg.rsgServer.repos.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@CrossOrigin
public class TransactionsController {

    @Autowired
    private TransactionsRepository repo;


    public TransactionsController(TransactionsRepository repository)
    {
        this.repo = repository;
    }

    public TransactionsController()
    {

    }

    @PostMapping(path="/transactions") // Map ONLY POST Requests
    public @ResponseBody
    Transactions add (@RequestBody Transactions input) {

        input.setCreatedAt(LocalDateTime.now());

        Transactions save = repo.save(input);

        return save;
    }


    @GetMapping(path="/transactions")
    public @ResponseBody Iterable<Transactions> getAll() {

        return repo.findAll();
    }

    @GetMapping(path="/transactions/{id}")
    public @ResponseBody Transactions get(@PathVariable Long id) {

        return repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));
    }

    @PutMapping(path="/transactions/{id}")
    public @ResponseBody
    ResponseEntity update(@PathVariable Long id, @RequestBody  Transactions input) {

        Transactions item = repo.findById(id.intValue()).orElseThrow(() -> new RsgNotFoundException(id));

        item.setAmount(input.getAmount());
        item.setCardId(input.getCardId());
        item.setCategory(input.getCategory());
        item.setChecks(input.getChecks());
        item.setCity(input.getCity());
        item.setCountry(input.getCountry());
        item.setCurrency(input.getCurrency());
        item.setCustomerId(input.getCustomerId());
        item.setInvestecAccountNumber(input.getInvestecAccountNumber());
        item.setMerchantName(input.getMerchantName());
        item.setPayload(input.getPayload());
        item.setReference(input.getReference());
        item.setAllowed(input.getAllowed());

        final  Transactions output = repo.save(item);

        return ResponseEntity.ok(output);
    }

    @DeleteMapping("/transactions/{id}")
    public void delete(@PathVariable Long id) {
        repo.deleteById(id.intValue());
    }
}
