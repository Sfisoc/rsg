package com.rsg.rsgServer.Documents;

import com.rsg.rsgServer.models.*;

import java.util.List;

public class ClpOut {

    List<CardRuleSets> cardRuleSets;
    List<Cards> cards;
    List<Checks> checks;
    List<Rules> rules;
    List<RuleSets> ruleSets;
    List<Transactions> transactions;
    List<Users> users;

    public List<CardRuleSets> getCardRuleSets() {
        return cardRuleSets;
    }

    public void setCardRuleSets(List<CardRuleSets> cardRuleSets) {
        this.cardRuleSets = cardRuleSets;
    }

    public List<Cards> getCards() {
        return cards;
    }

    public void setCards(List<Cards> cards) {
        this.cards = cards;
    }

    public List<Checks> getChecks() {
        return checks;
    }

    public void setChecks(List<Checks> checks) {
        this.checks = checks;
    }

    public List<Rules> getRules() {
        return rules;
    }

    public void setRules(List<Rules> rules) {
        this.rules = rules;
    }

    public List<RuleSets> getRuleSets() {
        return ruleSets;
    }

    public void setRuleSets(List<RuleSets> ruleSets) {
        this.ruleSets = ruleSets;
    }

    public List<Transactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transactions> transactions) {
        this.transactions = transactions;
    }

    public List<Users> getUsers() {
        return users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }
}
