package com.rsg.rsgServer.repos;

import com.rsg.rsgServer.models.RuleSets;
import org.springframework.data.repository.CrudRepository;

public interface RulesSetsRepository extends CrudRepository<RuleSets, Integer> {
}
