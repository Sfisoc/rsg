package com.rsg.rsgServer.repos;

import com.rsg.rsgServer.models.Checks;
import org.springframework.data.repository.CrudRepository;

public interface ChecksRepository extends CrudRepository<Checks, Integer> {
}
