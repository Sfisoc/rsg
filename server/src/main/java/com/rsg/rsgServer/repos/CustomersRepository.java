package com.rsg.rsgServer.repos;

import com.rsg.rsgServer.models.Customers;
import org.springframework.data.repository.CrudRepository;

public interface CustomersRepository extends CrudRepository<Customers, Integer> {
}
