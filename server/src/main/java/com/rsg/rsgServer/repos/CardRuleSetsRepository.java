package com.rsg.rsgServer.repos;

import com.rsg.rsgServer.models.CardRuleSets;
import org.springframework.data.repository.CrudRepository;

public interface CardRuleSetsRepository extends CrudRepository<CardRuleSets, Integer> {
}
