package com.rsg.rsgServer.repos;

import com.rsg.rsgServer.models.Rules;
import org.springframework.data.repository.CrudRepository;

public interface RulesRepository extends CrudRepository<Rules, Integer> {
}
