package com.rsg.rsgServer.repos;

import com.rsg.rsgServer.models.Transactions;
import org.springframework.data.repository.CrudRepository;

public interface TransactionsRepository extends CrudRepository<Transactions, Integer> {
}
