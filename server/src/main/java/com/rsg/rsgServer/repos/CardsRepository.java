package com.rsg.rsgServer.repos;

import com.rsg.rsgServer.models.Cards;
import org.springframework.data.repository.CrudRepository;

public interface CardsRepository extends CrudRepository<Cards, Integer> {
}
