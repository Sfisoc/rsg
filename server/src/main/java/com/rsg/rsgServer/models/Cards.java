package com.rsg.rsgServer.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Cards")
public class Cards {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "customerId", referencedColumnName = "id")
    private Customers customerId;

    @OneToMany(mappedBy = "cardId")
    @JsonIgnore
    private Set<Transactions> transactions = new HashSet<>();

    @OneToMany(mappedBy = "cardId")
    @JsonIgnore
    private Set<CardRuleSets> cardRuleSets = new HashSet<>();

    @Column(nullable = false,name = "investecClientId")
    private String investecClientId;
    @Column(nullable = false,name = "investecSecret")
    private String investecSecret;
    @Column(nullable = false,name = "investecAccountNumber")
    private String investecAccountNumber;
    @Column(nullable = false,name = "cardHolderName")
    private String cardHolderName;

    @Column(nullable = false,name = "updatedAt")
    private LocalDateTime updatedAt;
    @Column(nullable = false, name = "createdAt")
    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customers getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customers customerId) {
        this.customerId = customerId;
    }

    public Set<Transactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transactions> transactions) {
        this.transactions = transactions;
    }

    public Set<CardRuleSets> getCardRuleSets() {
        return cardRuleSets;
    }

    public void setCardRuleSets(Set<CardRuleSets> cardRuleSets) {
        this.cardRuleSets = cardRuleSets;
    }

    public String getInvestecClientId() {
        return investecClientId;
    }

    public void setInvestecClientId(String investecClientId) {
        this.investecClientId = investecClientId;
    }

    public String getInvestecSecret() {
        return investecSecret;
    }

    public void setInvestecSecret(String investecSecret) {
        this.investecSecret = investecSecret;
    }

    public String getInvestecAccountNumber() {
        return investecAccountNumber;
    }

    public void setInvestecAccountNumber(String investecAccountNumber) {
        this.investecAccountNumber = investecAccountNumber;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
