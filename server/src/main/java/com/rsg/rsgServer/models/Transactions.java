package com.rsg.rsgServer.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Transactions")
public class Transactions {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "customerId", referencedColumnName = "id")
    private Customers customerId;

    @ManyToOne
    @JoinColumn(name = "cardId", referencedColumnName = "id")
    private Cards cardId;

    @OneToMany(mappedBy = "transactionId")
    @JsonIgnore
    private Set<Checks> checks = new HashSet<>();

    @Column(nullable = false,name = "investecAccountNumber")
    private String investecAccountNumber;

    @Column(nullable = false,name = "reference")
    private String reference;
    @Column(name = "amount")
    private Double amount;
    @Column(nullable = false,name = "currency")
    private String currency;
    @Column(nullable = false,name = "merchantName")
    private String merchantName;
    @Column(nullable = false,name = "category")
    private String category;
    @Column(nullable = false,name = "city")
    private String city;
    @Column(nullable = false,name = "country")
    private String country;
    @Column(name = "payload")
    private String payload;

    @Column(name = "allowed")
    private int allowed;

    @Column(nullable = false, name = "createdAt")
    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customers getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customers customerId) {
        this.customerId = customerId;
    }

    public Cards getCardId() {
        return cardId;
    }

    public void setCardId(Cards cardId) {
        this.cardId = cardId;
    }

    public Set<Checks> getChecks() {
        return checks;
    }

    public void setChecks(Set<Checks> checks) {
        this.checks = checks;
    }

    public String getInvestecAccountNumber() {
        return investecAccountNumber;
    }

    public void setInvestecAccountNumber(String investecAccountNumber) {
        this.investecAccountNumber = investecAccountNumber;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public int getAllowed() {
        return allowed;
    }

    public void setAllowed(int allowed) {
        this.allowed = allowed;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
