package com.rsg.rsgServer.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "CardRuleSets")
public class CardRuleSets {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "customerId", referencedColumnName = "id")
    private Customers customerId;

    @ManyToOne
    @JoinColumn(name = "cardId", referencedColumnName = "id")
    private Cards cardId;

    @ManyToOne
    @JoinColumn(name = "ruleSetId", referencedColumnName = "id")
    private RuleSets ruleSetId;

    @Column(name = "enabled")
    private Boolean enabled;

    @Column(nullable = false, name = "updatedAt")
    private LocalDateTime updatedAt;
    @Column(nullable = false, name = "createdAt")
    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customers getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customers customerId) {
        this.customerId = customerId;
    }

    public Cards getCardId() {
        return cardId;
    }

    public void setCardId(Cards cardId) {
        this.cardId = cardId;
    }

    public RuleSets getRuleSetId() {
        return ruleSetId;
    }

    public void setRuleSetId(RuleSets ruleSetId) {
        this.ruleSetId = ruleSetId;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
