package com.rsg.rsgServer.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Customers")
public class Customers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "slackUrl", nullable = false)
    private String slackUrl;

    @Column(name = "postUrl", nullable = false)
    private String postUrl;

    @Column(nullable = false, name = "updatedAt")
    private LocalDateTime updatedAt;
    @Column(nullable = false, name = "createdAt")
    private LocalDateTime createdAt;

    @OneToMany(mappedBy = "customerId")
    @JsonIgnore
    private Set<Cards> cards = new HashSet<>();

    @OneToMany(mappedBy = "customerId")
    @JsonIgnore
    private Set<Users> users = new HashSet<>();

    @OneToMany(mappedBy = "customerId")
    @JsonIgnore
    private Set<CardRuleSets> cardRuleSets = new HashSet<>();

    @OneToMany(mappedBy = "customerId")
    @JsonIgnore
    private Set<Checks> checks = new HashSet<>();

    @OneToMany(mappedBy = "customerId")
    @JsonIgnore
    private Set<Rules> rules = new HashSet<>();

    @OneToMany(mappedBy = "customerId")
    @JsonIgnore
    private Set<RuleSets> ruleSets = new HashSet<>();

    @OneToMany(mappedBy = "customerId")
    @JsonIgnore
    private Set<Transactions> transactions = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlackUrl() {
        return slackUrl;
    }

    public void setSlackUrl(String slackUrl) {
        this.slackUrl = slackUrl;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public Set<Cards> getCards() {
        return cards;
    }

    public void setCards(Set<Cards> cards) {
        this.cards = cards;
    }

    public Set<Users> getUsers() {
        return users;
    }

    public void setUsers(Set<Users> users) {
        this.users = users;
    }

    public Set<CardRuleSets> getCardRuleSets() {
        return cardRuleSets;
    }

    public void setCardRuleSets(Set<CardRuleSets> cardRuleSets) {
        this.cardRuleSets = cardRuleSets;
    }

    public Set<Checks> getChecks() {
        return checks;
    }

    public void setChecks(Set<Checks> checks) {
        this.checks = checks;
    }

    public Set<Rules> getRules() {
        return rules;
    }

    public void setRules(Set<Rules> rules) {
        this.rules = rules;
    }

    public Set<RuleSets> getRuleSets() {
        return ruleSets;
    }

    public void setRuleSets(Set<RuleSets> ruleSets) {
        this.ruleSets = ruleSets;
    }

    public Set<Transactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transactions> transactions) {
        this.transactions = transactions;
    }
}