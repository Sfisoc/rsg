package com.rsg.rsgServer.models;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "Checks")
public class Checks {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "customerId", referencedColumnName = "id")
    private Customers customerId;

    @ManyToOne
    @JoinColumn(name = "transactionId", referencedColumnName = "id")
    private Transactions transactionId;

    @ManyToOne
    @JoinColumn(name = "ruleId", referencedColumnName = "id")
    private Rules ruleId;

    @Column(name = "ruleName", nullable = false)
    private String ruleName;
    @Column(name = "minimumAmount")
    private Double minimumAmount;
    @Column(name = "maximumAmount")
    private Double maximumAmount;
    @Column(name = "currency")
    private String currency;
    @Column(name = "ruleAppliesOnlyToMerchantRegex")
    private String ruleAppliesOnlyToMerchantRegex;
    @Column(name = "ruleDoesntApplyToMerchantRegex")
    private String ruleDoesntApplyToMerchantRegex;
    @Column(name = "allowedMerchantNameRegex")
    private String allowedMerchantNameRegex;
    @Column(name = "blockedMerchantNameRegex")
    private String blockedMerchantNameRegex;
    @Column(name = "allowedCategories")
    private String allowedCategories;
    @Column(name = "blockedCategories")
    private String blockedCategories;
    @Column(name = "allowedCities")
    private String allowedCities;
    @Column(name = "blockedCities")
    private String blockedCities;
    @Column(name = "country")
    private String country;

    @Column(name = "allow")
    private Boolean allow;


    @Column(nullable = false, name = "createdAt")
    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customers getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customers customerId) {
        this.customerId = customerId;
    }

    public Transactions getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Transactions transactionId) {
        this.transactionId = transactionId;
    }

    public Rules getRuleId() {
        return ruleId;
    }

    public void setRuleId(Rules ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Double getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(Double minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public Double getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(Double maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRuleAppliesOnlyToMerchantRegex() {
        return ruleAppliesOnlyToMerchantRegex;
    }

    public void setRuleAppliesOnlyToMerchantRegex(String ruleAppliesOnlyToMerchantRegex) {
        this.ruleAppliesOnlyToMerchantRegex = ruleAppliesOnlyToMerchantRegex;
    }

    public String getRuleDoesntApplyToMerchantRegex() {
        return ruleDoesntApplyToMerchantRegex;
    }

    public void setRuleDoesntApplyToMerchantRegex(String ruleDoesntApplyToMerchantRegex) {
        this.ruleDoesntApplyToMerchantRegex = ruleDoesntApplyToMerchantRegex;
    }

    public String getAllowedMerchantNameRegex() {
        return allowedMerchantNameRegex;
    }

    public void setAllowedMerchantNameRegex(String allowedMerchantNameRegex) {
        this.allowedMerchantNameRegex = allowedMerchantNameRegex;
    }

    public String getBlockedMerchantNameRegex() {
        return blockedMerchantNameRegex;
    }

    public void setBlockedMerchantNameRegex(String blockedMerchantNameRegex) {
        this.blockedMerchantNameRegex = blockedMerchantNameRegex;
    }

    public String getAllowedCategories() {
        return allowedCategories;
    }

    public void setAllowedCategories(String allowedCategories) {
        this.allowedCategories = allowedCategories;
    }

    public String getBlockedCategories() {
        return blockedCategories;
    }

    public void setBlockedCategories(String blockedCategories) {
        this.blockedCategories = blockedCategories;
    }

    public String getAllowedCities() {
        return allowedCities;
    }

    public void setAllowedCities(String allowedCities) {
        this.allowedCities = allowedCities;
    }

    public String getBlockedCities() {
        return blockedCities;
    }

    public void setBlockedCities(String blockedCities) {
        this.blockedCities = blockedCities;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Boolean isAllow() {
        return allow;
    }

    public void setAllow(Boolean allow) {
        this.allow = allow;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
