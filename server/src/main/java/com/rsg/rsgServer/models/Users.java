package com.rsg.rsgServer.models;

import javax.persistence.*;
import java.time.LocalDateTime;

import static org.hibernate.sql.InFragment.NULL;

@Entity
@Table(name = "Users")
// This tells Hibernate to make a table out of this class
public class Users {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "customerId", referencedColumnName = "id")
	private Customers customerId;

	@Column(name = "email")
	private String email;
	@Column(name = "provider")
	private String provider;
	@Column(name = "uid")
	private String uid;
	@Column(name = "encrypted_password", columnDefinition = "",nullable = false)
	private String encrypted_password;
	@Column(name = "remember_created_at",columnDefinition = NULL,nullable = false)
	private LocalDateTime remember_created_at;
	@Column(name = "sign_in_count")
	private int sign_in_count;
	@Column(name = "current_sign_in_at")
	private LocalDateTime current_sign_in_at;
	@Column(name = "last_sign_in_at")
	private LocalDateTime last_sign_in_at;
	@Column(name = "current_sign_in_ip")
	private String current_sign_in_ip;
	@Column(name = "last_sign_in_ip")
	private String last_sign_in_ip;
	@Column(nullable = false, name = "updatedAt")
	private LocalDateTime updatedAt;
	@Column(nullable = false,name = "createdAt")
	private LocalDateTime createdAt;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Customers getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Customers customerId) {
		this.customerId = customerId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getEncrypted_password() {
		return encrypted_password;
	}

	public void setEncrypted_password(String encrypted_password) {
		this.encrypted_password = encrypted_password;
	}

	public LocalDateTime getRemember_created_at() {
		return remember_created_at;
	}

	public void setRemember_created_at(LocalDateTime remember_created_at) {
		this.remember_created_at = remember_created_at;
	}

	public int getSign_in_count() {
		return sign_in_count;
	}

	public void setSign_in_count(int sign_in_count) {
		this.sign_in_count = sign_in_count;
	}

	public LocalDateTime getCurrent_sign_in_at() {
		return current_sign_in_at;
	}

	public void setCurrent_sign_in_at(LocalDateTime current_sign_in_at) {
		this.current_sign_in_at = current_sign_in_at;
	}

	public LocalDateTime getLast_sign_in_at() {
		return last_sign_in_at;
	}

	public void setLast_sign_in_at(LocalDateTime last_sign_in_at) {
		this.last_sign_in_at = last_sign_in_at;
	}

	public String getCurrent_sign_in_ip() {
		return current_sign_in_ip;
	}

	public void setCurrent_sign_in_ip(String current_sign_in_ip) {
		this.current_sign_in_ip = current_sign_in_ip;
	}

	public String getLast_sign_in_ip() {
		return last_sign_in_ip;
	}

	public void setLast_sign_in_ip(String last_sign_in_ip) {
		this.last_sign_in_ip = last_sign_in_ip;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}
}
