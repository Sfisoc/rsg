package com.rsg.rsgServer.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Rules")
public class Rules {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "customerId", referencedColumnName = "id")
    private Customers customerId;

    @ManyToOne
    @JoinColumn(name = "ruleSetId", referencedColumnName = "id")
    private RuleSets ruleSetId;

    @OneToMany(mappedBy = "ruleId")
    @JsonIgnore
    private Set<Checks> checks = new HashSet<>();

    @Column(nullable = false, name = "name")
    private String name;

    @Column(name = "minimumAmount")
    private Double minimumAmount;

    @Column(name = "maximumAmount")
    private Double maximumAmount;

    @Column(name = "currency")
    private String currency;

    @Column(name = "ruleAppliesOnlyToMerchantRegex")
    private String ruleAppliesOnlyToMerchantRegex;

    @Column(name = "ruleDoesntApplyToMerchantRegex")
    private String ruleDoesntApplyToMerchantRegex;

    @Column(name = "allowedMerchantNameRegex")
    private String allowedMerchantNameRegex;

    @Column(name = "blockedMerchantNameRegex")
    private String blockedMerchantNameRegex;

    @Column(name = "allowedCategories")
    private String allowedCategories;

    @Column(name = "blockedCategories")
    private String blockedCategories;

    @Column(name = "allowedCities")
    private String allowedCities;

    @Column(name = "blockedCities")
    private String blockedCities;

    @Column(name = "country")
    private String country;

    @Column(nullable = false, name = "updatedAt")
    private LocalDateTime updatedAt;
    @Column(nullable = false, name = "createdAt")
    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customers getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customers customerId) {
        this.customerId = customerId;
    }

    public RuleSets getRuleSetId() {
        return ruleSetId;
    }

    public void setRuleSetId(RuleSets ruleSetId) {
        this.ruleSetId = ruleSetId;
    }

    public Set<Checks> getChecks() {
        return checks;
    }

    public void setChecks(Set<Checks> checks) {
        this.checks = checks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(Double minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public Double getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(Double maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRuleAppliesOnlyToMerchantRegex() {
        return ruleAppliesOnlyToMerchantRegex;
    }

    public void setRuleAppliesOnlyToMerchantRegex(String ruleAppliesOnlyToMerchantRegex) {
        this.ruleAppliesOnlyToMerchantRegex = ruleAppliesOnlyToMerchantRegex;
    }

    public String getRuleDoesntApplyToMerchantRegex() {
        return ruleDoesntApplyToMerchantRegex;
    }

    public void setRuleDoesntApplyToMerchantRegex(String ruleDoesntApplyToMerchantRegex) {
        this.ruleDoesntApplyToMerchantRegex = ruleDoesntApplyToMerchantRegex;
    }

    public String getAllowedMerchantNameRegex() {
        return allowedMerchantNameRegex;
    }

    public void setAllowedMerchantNameRegex(String allowedMerchantNameRegex) {
        this.allowedMerchantNameRegex = allowedMerchantNameRegex;
    }

    public String getBlockedMerchantNameRegex() {
        return blockedMerchantNameRegex;
    }

    public void setBlockedMerchantNameRegex(String blockedMerchantNameRegex) {
        this.blockedMerchantNameRegex = blockedMerchantNameRegex;
    }

    public String getAllowedCategories() {
        return allowedCategories;
    }

    public void setAllowedCategories(String allowedCategories) {
        this.allowedCategories = allowedCategories;
    }

    public String getBlockedCategories() {
        return blockedCategories;
    }

    public void setBlockedCategories(String blockedCategories) {
        this.blockedCategories = blockedCategories;
    }

    public String getAllowedCities() {
        return allowedCities;
    }

    public void setAllowedCities(String allowedCities) {
        this.allowedCities = allowedCities;
    }

    public String getBlockedCities() {
        return blockedCities;
    }

    public void setBlockedCities(String blockedCities) {
        this.blockedCities = blockedCities;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
