package com.rsg.rsgServer.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "RuleSets")
public class RuleSets {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "customerId", referencedColumnName = "id")
    private Customers customerId;

    @OneToMany(mappedBy = "ruleSetId")
    @JsonIgnore
    private Set<CardRuleSets> cardRuleSets = new HashSet<>();

    @OneToMany(mappedBy = "ruleSetId")
    @JsonIgnore
    private Set<Rules> rules = new HashSet<>();

    @Column(nullable = false,name = "name")
    private String name;
    @Column(name = "deleted")
    private int deleted;

    @Column(nullable = false,name = "updatedAt")
    private LocalDateTime updatedAt;
    @Column(nullable = false,name = "createdAt")
    private LocalDateTime createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Customers getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customers customerId) {
        this.customerId = customerId;
    }

    public Set<CardRuleSets> getCardRuleSets() {
        return cardRuleSets;
    }

    public void setCardRuleSets(Set<CardRuleSets> cardRuleSets) {
        this.cardRuleSets = cardRuleSets;
    }

    public Set<Rules> getRules() {
        return rules;
    }

    public void setRules(Set<Rules> rules) {
        this.rules = rules;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
