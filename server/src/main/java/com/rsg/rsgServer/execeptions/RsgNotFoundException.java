package com.rsg.rsgServer.execeptions;

public class RsgNotFoundException extends RuntimeException {
    public RsgNotFoundException(Long id) {
        super("Could not find employee " + id);
    }
}
