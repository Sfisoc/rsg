# CardShark

Welcome to card sharp application repo.

CardShark is a credit card transaction management system that allows you to intelligently restrict and manage the use of Investec credit cards. You do this by creating sets of rules that, in real time, allow or deny transactions.

A conceptual overview is available in the PowerPoint deck located in the same folder as this file.

The application consist of a simple UI to manage cards and rules, as well as server side components that consider each transaction in the context of the the rules defined using the user interface. Developer installation notes are provied below.

**Note:** This solution is live and useable. We encourage you to experiment with the solution for yourself. See "try it" below.

## Overview

1. Link cards you want to manage to CardShark.
1. Define rules that regulate card transactions.
1. Tie card transactions into your corporate processes via HTTP callbacks and Slack

## Try It

1. Visit https://dev.d2slfnum00jv29.amplifyapp.com/
1. Enter your email address, and then, when you receive a PIN, enter that. The system will provision a company ("ACME Inc.") which you can rename using the settings icon.
1. Create a rule set, and then configure some rules for that rule set (see "Transaction Assessment Rules" below).
1. Once you have created a rule set and rules, click "Cards" and register a card. The Investec Account number is critical as this is used to relate credit card transactions to the card you have just defined.
1. Then, on the card, click "Edit Rule Sets" and link the card to one or more sets of rules.
1. As yet, there is not automated way to provision programable banking functionality. Detailed instructions are available in the app (and below). Essentially, end users must, for now, paste 24 lines of code into the Programmable Banking editor.
1. Lastly, you can configure HTTP or Slack integrations using the options on the Settings screen.

## Use Cases

### Sales Team Management

Sales team manager gives all his sales agents Investec credit cards for work related expenses. She sets hard and soft limits to their spend (where the agent is notified that they are approaching their limits, and where the limit is actually reached). Then, she configures rules:

1. Transactions only in each agent's home city.
1. Only hospitality related transactions (restuarants etc).
1. Maximum transaction amounts of R1000.

### Teenager Management

This scenario is illustrated in screen shots on slides six and seven of the PowerPoint available in the root of this repository.

1. Transactions of no more then R500 at Woolworths.
1. Transactions of no more than R300 at Pick n Pay
1. All other transactions, less than R150.
1. No transactions at Mavericks!
1. No booze or liquor related sales.


## Transaction Assessment Methodology

For any transaction, the system will:

1. Use the card account number to look up applicable, enabled, `rule sets`.
1. For those rule sets, the system will pull all undeleted `rules`.
1. It will the iterate over the rules, assessing each one as described below.

Each rule can return:

1. `True`: Indicating that this rule is happy for the transaction to be authorised.
2. `False`: Indicating that the rule does not want the transaction authorised.
3. `Null`: Indicating that the rule says nothing about the transaction.

For a transaction to be authorised, at least one rule must return `true`, and no transaction may return `false`.

There are two parts to each rule:

1. Rules that apply to specific merchants. Use these rules to define rules that apply at specific merchants, or at all other merchants.
1. Basic rules.

Rules that apply to merchants include:

* Minimum acceptable value
* Maximum acceptable value
* Country
* Currency

Rules that apply generally include:

* allowedMerchantNameRegex: Allow this transaction if the merchant name matches this regex.
* blockedMerchantNameRegex: Stop this transaction if the merchant name matches this regex.
* allowedCategories: Transactions are categories by Investec. The code expects an array (JSON encoded) of allowed categories (eg `['coffee shops', 'bakeries']`). Thransactions that fall into these categories are allowed.
* blockedCategories: The opposite.
* allowedCities: Similarly, you can provide an array of allowed cities: `['Cape Town', 'Johannesburg']`
* blockedCities: Or, dissallowed cities: ['Durban']

### Examples

To assist developers, these examples are given using SQL. However, you can equally, configure them using the user interface.

#### Store limits

Allow transactions of up to R500 at Woolworths, but no more than R300 at Pick n Pay:
````
insert into Rules (customerId, ruleSetId, name, ruleAppliesOnlyToMerchantRegex, maximumAmount, updatedAt, createdAt) values (9, 7, 'Woolworths', 'Wool', 500.00, now(), now());
insert into Rules (customerId, ruleSetId, name, ruleAppliesOnlyToMerchantRegex, maximumAmount, updatedAt, createdAt) values (9, 7, 'Pick n Pay', 'Pick', 300.00, now(), now());
````

To allow a basic limit for all other stores, add the following:
````
insert into Rules (customerId, ruleSetId, name, ruleDoesntApplyToMerchantRegex, maximumAmount, updatedAt, createdAt) values (9, 7, 'Max for non-food retailers', 'Pick|Wool', 150.00, now(), now());
````

#### Block a merchant
Prevent any transactions that happen at Mavericks:
````
insert into Rules (customerId, ruleSetId, name, blockedMerchantNameRegex, updatedAt, createdAt) values (9, 7, 'Mavericks', 'Mavericks', now(), now());
````

#### Block categories of transactions
Don't allow transactions at alcohol related stores (NB: I have no idea if these category names are right):
````
insert into Rules (customerId, ruleSetId, name, blockedCategories, updatedAt, createdAt) values (9, 7, 'No Booze', '["liquor", "booze"]', now(), now());
````
Note that category definitions can be found in the Investec programmable banking editor, in the `merchants.json` file.

## Deployment

### Programable Banking API

Unfortunately, every card must be manually provisioned by the account owner:

1. Register your Investec account as part of the Programmable Banking initiative.
1. Login, and click "Start programming your bank cards now".
1. Click on the image of the credit card.
1. When the editor loads, locate main.js on the left hand side. Click it to open the file.
1. If you are asked if you would like to reload changes from a previous session, say yes.
1. Replace the code in the editor, with the code below.
1. Click Save Changes.
1. Click Deply code to card.
1. And you should be rewarded (briefly!) with the message: "Code published!".

### Database(db)

The application is built against a MYSQL database. A script to generate this database is included in this repo at `scripts/rsg.sql`. The script also includes sample data that can be used to seed the database with some basic data.

### Server

#### Docker

There exists a DOCKERFILE to spin up a docker container. Note that if you adopt this stratey, you will need to update the database url in application.properties file.

The application uses spring boot framework (located at server folder).

#### Lambdas

The production environment runs using AWS lambdas. Simplest is to install AWS's SAM deployment tools. Because we are working with MYSQL, you will need to build the libraries that support MYSQL:

Follow these instructions: https://blog.shikisoft.com/ruby-aws-lambda-sam-cli-rds-mysql/

Then, to test and deploy the code, from within `/lambdas/rsg`:

```
sam build --use-container
sam deploy --guided
```

You can test the solution locally by running, in `lambdas/rsg` `./run.rb rsg assess.json`.


### UI

The user interface has been built using Vue 2.6. Simplese is to install Vue, and then, using Vue UI, to import the project.
